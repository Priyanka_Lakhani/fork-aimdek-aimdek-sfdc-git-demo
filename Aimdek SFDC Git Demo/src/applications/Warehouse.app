<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Warehouse</label>
    <tab>Merchandise__c</tab>
    <tab>Invoice_Statement__c</tab>
    <tab>Problem_Type__c</tab>
    <tab>Service_Tracker_Detail__c</tab>
    <tab>Close_the_loop__c</tab>
</CustomApplication>
