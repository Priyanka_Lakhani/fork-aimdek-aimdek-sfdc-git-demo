<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Program_Name</fullName>
        <field>Name</field>
        <formula>Program_Code__c &amp;&quot; - &quot;&amp; Account__r.Name &amp; &quot; [&quot; &amp; TEXT(Description__c)&amp;&quot;]&quot;</formula>
        <name>Update Program Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Program%3A Stamp Name</fullName>
        <actions>
            <name>Update_Program_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
