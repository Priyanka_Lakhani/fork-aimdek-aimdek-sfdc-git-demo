<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Record_Type_Dealership</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Dealership</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account Record Type: Dealership</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Set_Number</fullName>
        <field>AccountNumber</field>
        <formula>Account_Number__c</formula>
        <name>Account: Set Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Set_Site</fullName>
        <field>Site</field>
        <formula>Account_Number__c</formula>
        <name>Account: Set Site</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account%3A Record Type</fullName>
        <actions>
            <name>Account_Record_Type_Dealership</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Dealership / Tow Destination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>contains</operation>
            <value>EV Charge Station</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>GPBR</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Set Details</fullName>
        <actions>
            <name>Account_Set_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Set_Site</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
