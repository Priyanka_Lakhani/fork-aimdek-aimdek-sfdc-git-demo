trigger CaseGPBRchanged on Case (before Update,before Insert) {
    for(Case cs: Trigger.new){
        if(Trigger.isInsert){
            if(cs.GPBR__c != null && cs.GPBR__c != ''){
                cs.GPBR_Entered_by_Case_Owner__c = true;
            }
        }
        if(Trigger.isUpdate){
            if((cs.GPBR__c != '' && cs.GPBR__c != null ) && (trigger.oldmap.get(cs.id).GPBR__c == '' || trigger.oldmap.get(cs.id).GPBR__c == null) && cs.OwnerId == UserInfo.getUserId()){
                cs.GPBR_Entered_by_Case_Owner__c = true;
            }
        }
        
    }
}