trigger setAccountDetails on Account (before update) {
    
    for(Account a : trigger.new) {
        Date today = Date.today();
        
        if(a.Start_of_Day__c!=NULL) {
            a.Start_of_Day__c=Datetime.newInstance(today.year(), today.month(), today.day(), a.Start_of_Day__c.hour(), a.Start_of_Day__c.minute(), a.Start_of_Day__c.second());
        }
        if(a.End_of_Day__c!=NULL) {
            a.End_of_Day__c=Datetime.newInstance(today.year(), today.month(), today.day(), a.End_of_Day__c.hour(), a.End_of_Day__c.minute(), a.End_of_Day__c.second());
        }
        if(a.Location__Latitude__s!=trigger.oldMap.get(a.Id).Location__Latitude__s||a.Location__Longitude__s!=trigger.oldMap.get(a.Id).Location__Longitude__s) {
            a.Location_Last_Updated__c=System.now();
        }

        a.Hours_of_Operation_HTML__c = '';
        a.Hours_of_Operation_HTML__c += ('<strong>Mon:</strong> ' + (a.Monday__c!=NULL?a.Monday__c:'') + '<br/>');
        a.Hours_of_Operation_HTML__c += ('<strong>Tue:</strong> ' + (a.Tuesday__c!=NULL?a.Tuesday__c:'') + '<br/>');
        a.Hours_of_Operation_HTML__c += ('<strong>Wed:</strong> ' + (a.Wednesday__c!=NULL?a.Wednesday__c:'') + '<br/>');
        a.Hours_of_Operation_HTML__c += ('<strong>Thu:</strong> ' + (a.Thursday__c!=NULL?a.Thursday__c:'') + '<br/>');
        a.Hours_of_Operation_HTML__c += ('<strong>Fri:</strong> ' + (a.Friday__c!=NULL?a.Friday__c:'') + '<br/>');
        a.Hours_of_Operation_HTML__c += ('<strong>Sat:</strong> ' + (a.Saturday__c!=NULL?a.Saturday__c:'') + '<br/>');
        a.Hours_of_Operation_HTML__c += ('<strong>Sun:</strong> ' + (a.Sunday__c!=NULL?a.Sunday__c:'') + '');

    }

}