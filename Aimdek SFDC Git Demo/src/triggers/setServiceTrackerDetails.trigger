trigger setServiceTrackerDetails on Service_Tracker_Detail__c (after insert, before update) {
    if(trigger.isInsert) {
        List<Service_Tracker_Detail__c> st = new List<Service_Tracker_Detail__c>();
        for(Service_Tracker_Detail__c s : trigger.new) {
            if(s.Case__c==null) {
                st.add(new Service_Tracker_Detail__c(Id=s.Id));
            }
        }
        if(!st.isEmpty()) { update st; }
        
    } else {
        
        Date lowestDate = Date.today();
        Date highestDate = Date.today().addDays(-10);
        Set<String> clubs = new Set<String>();
        Set<String> call_ids = new Set<String>();
        Map<String, Id> cMap = new Map<String, Id>();
        Map<String, Id> cMapPr = new Map<String, Id>();
        for(Service_Tracker_Detail__c s : trigger.new) {
            if(s.Case__c==null) {
                cMap.put(s.UID__c, NULL);
                cMapPr.put(s.UID__c, NULL);
                call_ids.add(s.Call_Number__c);
                clubs.add(s.Servicing_Club_Code__c);
                if(s.Call_Date__c<lowestDate) { lowestDate=s.Call_Date__c; }
                if(s.Call_Date__c>highestDate) { highestDate=s.Call_Date__c; }
            }
        }
        clubs.remove(null);
        Set<Id> club_ids = new Set<Id>();
        if(!clubs.isEmpty()) {
            for(Account a : [Select Id from Account where RecordType.DeveloperName='Dispatch_Club' AND Account_Number__c in: clubs]) {
                club_ids.add(a.Id);
            }
        }
        cMap.remove(null);
        cMapPr.remove(null);
        call_ids.remove(null);
        
        if(!cMap.isEmpty()) {
            List<Case> casesToUpdate = new List<Case>();
            for(Case c : [Select Id, Service_Tracker_UID__c,Service_Tracker_UID_Program_Code__c, RE_Status_Date_Time__c from Case where Club_Call_Number__c<>null AND Club_Call_Number__c in: call_ids AND Club__c<>null AND (Club__c in: club_ids OR Program__r.Program_Code__c in: clubs) AND RE_Status_Date_Time__c<>null /*AND RE_Status_Date_Time__c >=: lowestDate AND RE_Status_Date_Time__c <=: highestDate*/ AND (Service_Tracker_UID__c in: cMap.keySet() OR Service_Tracker_UID_Program_Code__c in: cMap.keySet())]) {
                if(c.RE_Status_Date_Time__c.date() >= lowestDate && c.RE_Status_Date_Time__c.date() <= highestDate){
                    cMap.put(c.Service_Tracker_UID__c, c.Id);
                    cMapPr.put(c.Service_Tracker_UID_Program_Code__c, c.Id);
                }
            }
            
            for(Service_Tracker_Detail__c s : trigger.new) {
                if(s.Case__c==null && cMap.get(s.UID__c)!=null) {
                    s.Case__c=cMap.get(s.UID__c);
                    casesToUpdate.add(new Case(Id=s.Case__c, Service_Tracker_Detail__c=s.Id));
                }
                else if (s.Case__c==null && cMapPr.get(s.UID__c)!=null){
                    s.Case__c = cMapPr.get(s.UID__c);
                    casesToUpdate.add(new Case(Id=s.Case__c, Service_Tracker_Detail__c=s.Id));
                }
            }
            
            if(!casesToUpdate.isEmpty()) { update casesToUpdate; }
        }
    }
}