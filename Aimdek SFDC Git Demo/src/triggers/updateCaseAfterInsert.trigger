trigger updateCaseAfterInsert on Case (after insert, after update) {    

    List<Case> updateList = new List<Case>();
    Map<Id, Task> tMap = new Map<Id, Task>();
    
    for(Case c : trigger.new) {
        if(trigger.isInsert) { 
            if(c.Call_ID__c==NULL) { updateList.add(new Case(Id=c.Id)); }
        }
        if(trigger.isInsert || (trigger.isUpdate && c.Interaction_ID__c!=NULL && c.Interaction_ID__c!=trigger.oldMap.get(c.Id).Interaction_ID__c)) {
            if(c.Interaction_ID__c!=NULL&&c.Interaction_ID__c!='null'&&c.Interaction_ID__c!='') {
                tMap.put(c.Id, new Task(
                    OwnerId=c.OwnerId,
                    Subject=('Call ' + (c.RE_Status_Date_Time__c!=NULL?c.RE_Status_Date_Time__c:DateTime.now()).format()),
                    Type='Call',
                    ActivityDate=Date.today(),
                    CallObject=c.Interaction_ID__c,
                    WhatId=c.Id,
                    Status='Completed'
                ));
                Decimal dur = (DateTime.now().getTime()/1000)-((c.RE_Status_Date_Time__c!=NULL?c.RE_Status_Date_Time__c:DateTime.now()).getTime()/1000);
                tMap.get(c.Id).CallDurationInSeconds=Integer.valueOf(dur);
            }
        }
    }
    if(tMap.keySet().size()>0) { insert tMap.values(); }
    if(updateList.size()>0) { update updateList; }
}