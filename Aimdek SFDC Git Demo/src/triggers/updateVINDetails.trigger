trigger updateVINDetails on VIN__c (after insert, after update) {
    
    Set<Id> vids = new Set<Id>();
    
    for(VIN__c v : trigger.new) {
        VIN__c oldV = trigger.isUpdate?trigger.oldMap.get(v.Id):NULL;
        
        if(trigger.isInsert || (trigger.isUpdate && (v.Name!=oldV.Name||v.PIN__c!=oldV.PIN__c||v.License__c!=oldV.License__c||v.Plan_Name__c!=oldV.Plan_Name__c||v.First_Name__c!=oldV.First_Name__c||v.Last_Name__c!=oldV.Last_Name__c||v.Personal_First_Name__c!=oldV.Personal_First_Name__c||v.Personal_Last_Name__c!=oldV.Personal_Last_Name__c))) { vids.add(v.Id); }
    }
    
    if(!vids.isEmpty()) {
        Database.update(BatchUpdateVIN.refreshVINs(
            [SELECT Id, PIN__c, Year__c, Make__c, Model__c, License__c, Plan_Name__c, First_Name__c, Last_Name__c, Personal_First_Name__c, Personal_Last_Name__c, Name, Program__r.Call_Flow_Autocomplete_Type__c FROM VIN__c WHERE Id in: vids]
        ).values());
    }

}