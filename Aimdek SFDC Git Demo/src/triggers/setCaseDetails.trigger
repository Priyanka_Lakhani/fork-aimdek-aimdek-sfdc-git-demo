trigger setCaseDetails on Case (before insert, before update) {
    
    Map<String, Id> vMap = new Map<String, Id>();
    Map<String, Id> gpbrMap = new Map<String, Id>();
    Map<String, Territory_Coverage__c> coverageMap = new Map<String, Territory_Coverage__c>();


    CA__c settings = CA__c.getOrgDefaults();
    Integer last_case_no = (Integer.valueOf(settings.Last_Case_Number__c)-1);
    Integer offset = 1000;
    User u = [Select Id, External_ID__c from User where Id=:userInfo.getUserId() LIMIT 1];
    Boolean isAPI = FALSE;

    try {
        isAPI = (CA__c.getOrgDefaults().API_User_IDs__c.indexOf(String.valueOf(userinfo.getUserId()).left(15))!=-1);
    } catch(Exception e) {
        isAPI=FALSE;
    }
    
    id mtclub = null;
    try
    { mtclub = [select id from account where name ='JLR Mobile Technician' and recordtype.name='Dispatch Club' and status__c='Active' limit 1].id;}
    catch(system.exception ex){}
    
    id aaaclub = null;
    
    try
    { aaaclub = [select id from account where name ='AAA Dispatch' and recordtype.name='Dispatch Club' limit 1].id;}
    catch(system.exception ex){}
    
    for(Case c : trigger.new) {
        
        Boolean clubChanged = (trigger.isUpdate && c.Club__c!=trigger.oldMap.get(c.Id).Club__c);
        if(c.Club_Total_Service_Charge__c==null||c.Club_Total_Service_Charge__c<0.01||clubChanged) { c.Club_Total_Service_Charge__c = c.Default_Service_Cost__c; }
        if(c.Club_Total_Tow_Charge__c==null||c.Club_Total_Tow_Charge__c<0.01||clubChanged) { c.Club_Total_Tow_Charge__c = c.Default_Tow_Cost__c; }
        if(c.Club_Cost_Per_KM__c==null||c.Club_Cost_Per_KM__c<0.01||clubChanged) { c.Club_Cost_Per_KM__c = c.Default_Cost_Per_KM__c; }
        
        if(c.ETA_Date__c==null) { c.ETA_Date__c=Date.today(); }
        if(c.Clear_Destination__c) {
            c.Dealership__c=null;
            c.Destination_City__c='';
            c.Destination_Country__c='';
            c.Destination_Location__Latitude__s=null;
            c.Destination_Location__Longitude__s=null;
            c.Destination_Name__c='';
            c.Destination_Phone__c='';
            c.Destination_Postal_Code__c='';
            c.Destination_Province__c='';
            c.Destination_Street__c='';
            c.Tow_Reason__c='';
        }
        if(c.Close_Reason_Non_Roadside__c!=NULL&&c.Close_Reason_Non_Roadside__c!=trigger.oldMap.get(c.Id).Close_Reason_Non_Roadside__c) { 
            c.Kill_Code__c=c.Close_Reason_Non_Roadside__c; 
        }
        c.Wait__c=c.ETA__c;
        if(c.RE_Status_Date_Time__c==NULL) { 
            c.RE_Status_Date_Time__c=System.now(); 
            c.RE_Employee__c=u.Id; 
            c.RE_Employee_Code__c=u.External_ID__c;
        }
        if(trigger.isUpdate) {
            if(c.Call_ID__c==NULL) {
                Integer current_case_no = Integer.valueOf(c.CaseNumber);
                c.Call_ID__c = (String.valueOf((current_case_no-last_case_no+offset))+'%2aCA%2a'+c.CAKEY_Date__c);
            }
        }
        if(!isAPI) {
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.RE_Status__c) { c.RE_Status_Date_Time__c=System.now(); c.RE_Employee__c=u.Id; c.RE_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.DI_Status__c) { c.DI_Status_Date_Time__c=System.now(); c.DI_Employee__c=u.Id; c.DI_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.ER_Status__c) { c.ER_Status_Date_Time__c=System.now(); c.ER_Employee__c=u.Id; c.ER_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.OL_Status__c) { c.OL_Status_Date_Time__c=System.now(); c.OL_Employee__c=u.Id; c.OL_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.SP_Status__c) { c.SP_Status_Date_Time__c=System.now(); c.SP_Employee__c=u.Id; c.SP_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.TW_Status__c) { c.TW_Status_Date_Time__c=System.now(); c.TW_Employee__c=u.Id; c.TW_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.CL_Status__c) { c.CL_Status_Date_Time__c=System.now(); c.CL_Employee__c=u.Id; c.CL_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.KI_Status__c) { c.KI_Status_Date_Time__c=System.now(); c.KI_Employee__c=u.Id; c.KI_Employee_Code__c=u.External_ID__c; }
            if((trigger.isInsert||(trigger.isUpdate&&trigger.oldMap.get(c.Id).Status!=c.Status))&&c.Status==settings.XX_Status__c) { c.XX_Status_Date_Time__c=System.now(); c.XX_Employee__c=u.Id; c.XX_Employee_Code__c=u.External_ID__c; }
        }

        try { c.Phone_Stripped__c=c.Phone__c.replaceAll('\\D',''); } catch(Exception e) { c.Phone_Stripped__c=''; }
        
        String coverageKey1 = (c.City__c!=NULL?c.City__c:'')+''+(c.Province__c!=NULL?c.Province__c:'');
        c.Coverage_Key__c=coverageKey1.toUpperCase();
        
        coverageMap.put(c.Coverage_Key__c, NULL);
        
        c.Client_Code__c=c.Client_Code_Formula__c;
        if(c.VIN_Member_ID__c==NULL) { c.VIN__c=NULL; }
        if(c.GPBR__c==NULL) { c.GPBR_Station__c=NULL; }
        if(c.VIN_Member_ID__c!=null) { 
            if (c.EHICall__c)
                vMap.put('930-' + c.VIN_Member_ID__c.toUpperCase(), NULL);
            else if(c.client_code__c =='582')
                vMap.put(c.client_code__c+'-' + c.VIN_Member_ID__c.toUpperCase(), NULL);
            else
            vMap.put(c.VIN_Member_ID__c.toUpperCase(), NULL); 
        }
        
        gpbrMap.put(c.GPBR__c, NULL);
    }
    coverageMap.remove(null);
    vMap.remove(null);
    gpbrMap.remove(null);
    
    System.debug('vMap: ' + vMap);
    
    if(!coverageMap.isEmpty()) {
        for(Territory_Coverage__c t : [Select Id, Club__c, Club__r.Total_Service_Cost__c, Club__r.Total_Tow_Charge__c, Club__r.Cost_Per_KM__c, Club__r.Code_Red__c, Club__r.Off_Hours__c, Club__r.Off_Hours_Club__c, Club__r.Off_Hours_Club__r.Code_Red__c, Club__r.Off_Hours_Club__r.Off_Hours__c, Coverage_Key__c, Coverage_Key_Short__c from Territory_Coverage__c where (Coverage_Key__c in: coverageMap.keySet() OR Coverage_Key_Short__c in: coverageMap.keySet())]) {
            coverageMap.put(t.Coverage_Key__c, t);
            coverageMap.put(t.Coverage_Key_Short__c, t);
        }
    }
    if(!vMap.isEmpty()) {
        for(VIN__c v : [Select Id, UID__c,Name from VIN__c where UID__c in: vMap.keySet()]) {
            if (v.UID__C.contains('-'))
            {
                if (!vMap.ContainsKey(v.Name.toUpperCase()))
                    vMap.put(v.Name.toUpperCase(),v.Id);
            }
            else
                vMap.put(v.UID__c.toUpperCase(), v.Id);
        }
        System.debug('Query vMap: ' + vMap);
    }
    if(!gpbrMap.isEmpty()) {
        for(Account v : [Select Id, GPBR__c from Account where GPBR__c in: gpbrMap.keySet() AND Type='GPBR']) {
            gpbrMap.put(v.GPBR__c, v.Id);
        }
    }
    for(Case c : trigger.new) {
        if (c.ManualSpotting__c != null && c.ManualSpotting__c)
        {
            if (c.Country__c == 'US')
                c.ManualSpotting__c = false;
            else
                continue;
        }
            
        //c.Club__c = null;
        if(c.VIN_Member_ID__c!=NULL) { try { c.VIN__c=vMap.get(c.VIN_Member_ID__c.toUpperCase()); } catch(Exception e) { System.debug('Could Not Find VIN: ' + c.VIN_Member_ID__c); } }
        if(c.GPBR__c!=NULL) { try { c.GPBR_Station__c=gpbrMap.get(c.GPBR__c); } catch(Exception e) {} }
                
        system.debug('c.sponsorId__c:' + c.sponsorId__c);
        system.debug('c.programId__c:' + c.programId__c);
        
        boolean success = false;
        if (c.JLRTriageMTRequired__c=='Yes' && c.JLRTriageLowSpeed__c == 'Yes' && c.JLRTriageAgent__c =='Yes' && c.JLRMTFound__c && c.MobTechDenied__c != 'Yes')
            success = true;
            
        if (!success && aaaclub!=null && c.Country__c == 'US' && (c.sponsorId__c != null && c.sponsorId__c!='') && (c.programId__c != null && c.programId__c!=''))
        {
            if (c.Club__c==aaaclub)
                success = true;
            else
            {
                try
                {
                    // Commented below line for taskid 8231437, CallId set null whenever user try to change/remove club manually for AAA
                    //c.club_call_number__c = '';
                    c.Club__c = aaaclub;
                    success = true;
                }
                catch(System.Exception ex){}
            }
        }
        
        if (!success)
        {
            if (c.Club__c == mtClub || c.Club__c == aaaclub)
                c.Club__c=null;
                
            Territory_Coverage__c temp = coverageMap.get(c.Coverage_Key__c);
            //if((c.Club__c==NULL||c.Club_Off_Hours__c) && temp!=NULL) { 
            if(c.Club__c==NULL && temp!=NULL) {          
                if(temp.Club__c!=NULL && temp.Club__r.Off_Hours__c && temp.Club__r.Off_Hours_Club__c!=NULL) {
                    c.Club__c=temp.Club__r.Off_Hours_Club__c;
                    c.Red_Flag__c=temp.Club__r.Off_Hours_Club__r.Code_Red__c;
                } else {
                    c.Club__c=temp.Club__c; 
                    c.Red_Flag__c=temp.Club__r.Code_Red__c;
                }
            }
        }
    }

}