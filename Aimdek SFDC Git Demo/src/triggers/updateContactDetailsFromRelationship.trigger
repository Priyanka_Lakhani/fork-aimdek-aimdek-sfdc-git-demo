trigger updateContactDetailsFromRelationship on Relationship_Rating__c (after insert, after update, after delete) {

    Map<Id, Contact> cMap = new Map<Id, Contact>();
    for(Relationship_Rating__c r : trigger.isDelete?trigger.Old:trigger.new) {
        cMap.put(r.Contact__c, new Contact(Id=r.Contact__c, Relationship_Rating__c='', Relationship_Rating_Date__c=NULL));
    }
    
    for(Contact c : [Select Id, (Select Date__c, Relationship_Rating__c from Relationship_Ratings__r ORDER BY Date__c DESC LIMIT 1) from Contact where Id in: cMap.keySet()]) {
        if(c.Relationship_Ratings__r!=NULL&&c.Relationship_Ratings__r.size()!=0) {
            cMap.get(c.Id).Relationship_Rating__c = c.Relationship_Ratings__r[0].Relationship_Rating__c;
            cMap.get(c.Id).Relationship_Rating_Date__c = c.Relationship_Ratings__r[0].Date__c;            
        }
    }
    update cMap.values();
}