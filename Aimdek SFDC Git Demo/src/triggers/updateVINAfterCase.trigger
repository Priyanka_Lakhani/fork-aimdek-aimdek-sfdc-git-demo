trigger updateVINAfterCase on Case (after insert, after update) {
    
    String p_update = CA__c.getOrgDefaults().Call_Flow_Personal_Update_Request__c;
    Map<Id,VIN__c> vMap = new Map<Id,VIN__c>();
    
    for(Case c : trigger.new) {
        if(!c.isClosed && c.VIN__c!=NULL&&c.Miscellaneous_Call_Reason__c==p_update) {
            vMap.put(c.VIN__c, new VIN__c(
                Id=c.VIN__c,
                Personal_Phone__c=c.Phone__c,
                Personal_Email__c=c.Email__c,
                Personal_Street__c=c.Home_Street__c,
                Personal_City__c=c.Home_City__c,
                Personal_Province__c=c.Home_Province__c,
                Personal_Postal_Code__c=c.Home_Postal_Code__c
            ));
        }
    }
    if(vMap.keySet().size()>0) {
        Database.update(vMap.values());

        Database.update(BatchUpdateVIN.refreshVINs([SELECT Id, PIN__c, Year__c, Make__c, Model__c, License__c, Plan_Name__c, First_Name__c, Last_Name__c, Personal_First_Name__c, Personal_Last_Name__c, Name, Program__r.Call_Flow_Autocomplete_Type__c FROM VIN__c WHERE Id in: vMap.keySet()]).values());
        
    }
}