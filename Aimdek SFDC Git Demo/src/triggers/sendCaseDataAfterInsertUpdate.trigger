trigger sendCaseDataAfterInsertUpdate on Case (after insert, after update) {
    CA__c settings = CA__c.getOrgDefaults();
    List<Case> updateList = new List<Case>();
    User u = [Select Id, External_ID__c from User where Id=:userInfo.getUserId() LIMIT 1];
    Boolean isAPI = FALSE;

    try {
        isAPI = (CA__c.getOrgDefaults().API_User_IDs__c.indexOf(String.valueOf(userinfo.getUserId()).left(15))!=-1);
    } catch(Exception e) {
        isAPI=FALSE;
    }

    for(Case c : trigger.new){
        if(trigger.isInsert || trigger.isUpdate){    
            try{
                if((c.client_code__c == '960' && c.clientcasenumber__c != null)){
                    if(c.ETA__c == null && c.Club_code__c != null){
                        Account clubAccount = [Select id,name,account_number__c,ETA__c from account where account_number__c=:c.Club_code__c limit 1];
                        if(clubAccount != null && clubAccount.ETA__c != null && clubAccount.ETA__c != 0){
                            Decimal eta = clubAccount.ETA__c;
                            updateList.add(new Case(Id=c.Id,ETA__c = eta));
                            if(updateList.size()>0) { update updateList; }
                        }
                    }
                    if(trigger.isInsert){
                        if(c.club_code__c!= null)
                            RestCallouts.dispatchCase(c.Id);
                        else
                            RestCallouts.sendNotDispatchEmailToMBC(c);
                    }
                    if (GlobalClass.firstRun){
                        GlobalClass.firstRun = false; 
                         //update call info
                         //Added condition for update call on vehicle-in-tow status-24-11-2017
                         if(trigger.isUpdate && Trigger.oldmap != null && Trigger.oldmap.get(c.id).Status != null && (Trigger.oldmap.get(c.id).Status == c.Status) && (c.Status == settings.DI_Status__c || c.Status == settings.ER_Status__c || c.Status == settings.OL_Status__c || c.Status == settings.TW_Status__c)){
                              // to prevent dispatch call on acklowdgement from Salesforce Integration-24-11-2017
                              if(!isAPI) {
                                 RestCallouts.updateServiceCall(c.Id);
                              }
                         }
                         //update timestamp
                         //Added condition to prevent update timestamp on spotted status-24-11-2017
                         if(trigger.isUpdate && Trigger.oldmap != null && Trigger.oldmap.get(c.id).Status != null && (Trigger.oldmap.get(c.id).Status != c.Status) && (c.Status != settings.RE_Status__c) && (c.Status != settings.SP_Status__c)){                                                                                            
                                String content = RestCallouts.getMercedesBenzBreakDownData(c.Id);
                                String host = ClientServicesSetting__c.getAll().get('mercedes-benz').EndPoint_URL__c;
                                String timestamp_url_path = '/mbc/updateStatus';
                                String closeCase_url_path = '/mbc/closeCase';
                                String end_point = '';
                                String reqMethod= '';
                                String username = ClientServicesSetting__c.getAll().get('mercedes-benz').UserName__c;
                                String password = ClientServicesSetting__c.getAll().get('mercedes-benz').Password__c;
                                end_point = host + timestamp_url_path;
                                reqMethod = 'POST';
                                if(c.Status != settings.DI_Status__c && c.Status != settings.XX_Status__c && c.Status != settings.CL_Status__c)
                                    RestCallouts.callout(end_point, username, password , content, c.id, reqMethod);
                                if(trigger.isUpdate && (c.Status == settings.CL_Status__c || c.Status == settings.XX_Status__c)){
                                    end_point = host + closeCase_url_path;
                                    reqMethod = 'PUT';
                                    RestCallouts.callout(end_point, username, password , content, c.id, reqMethod);
                                }  
                            }                                                                                                                             
                        }else{
                            if(trigger.isUpdate && Trigger.oldmap != null && Trigger.oldmap.get(c.id).Status != null && ((Trigger.oldmap.get(c.id).club_code__c == null && c.club_code__c != null)|| (Trigger.oldmap.get(c.id).club_code__c!= null && Trigger.oldmap.get(c.id).club_code__c != c.club_code__c)) && (c.Status == settings.RE_Status__c || c.Status == settings.SP_Status__c)){
                                  if(!isAPI && c.club_code__c!= null) {
                                        RestCallouts.dispatchCase(c.Id);
                                  }
                             }
                            System.debug('Already ran!');
                            return;
                        }
                    }
            }catch(System.Exception ex){
                System.debug('Exception:'+ex.getMessage());
            }
        }
    }
}