@RestResource(urlMapping='/SiriusXM_Listener/processDeviceRequest/*')
global with sharing class SiriusXM_Listener {
    @HttpPost
    global static void processDeviceRequest() {
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestRequest req = RestContext.request;
        RequestService reqObj = (RequestService)JSON.deserialize(req.requestBody.toString(),RequestService.class);    
        ResponseService resp = new ResponseService();  
        
        if(reqObj.location != null){
        
            resp.referenceId = reqObj.referenceId;
            resp.latitude = reqObj.location.geopoint.latitude;
            resp.longitude = reqObj.location.geopoint.longitude;
            resp.resultCode = '';
            String test = req.requestBody.toString();
            //TB temp workaround start
            String temp_var = '';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            temp_var += '1';
            //TB temp workaround start
            if(resp.referenceId != null && resp.referenceId != '' && resp.latitude != null && resp.longitude != null){
                
                try{
                
                    Case sirCase = [SELECT id,Terminate_Info__c, Breakdown_Location__latitude__s, Breakdown_Location__longitude__s, Street__c, City__c, Province__c, Postal_Code__c, Country__c FROM Case WHERE Sirius_XM_ID__c =: String.ValueOf(resp.referenceId) ORDER BY CreatedDate DESC LIMIT 1];
                    
                    sirCase.Breakdown_Location__latitude__s = resp.latitude;
                    sirCase.Breakdown_Location__longitude__s = resp.longitude;
                    sirCase.Terminate_Info__c = test;
                    Case temp = MobileIntegrationController.reverse_geocode(sirCase.Breakdown_Location__Latitude__s, sirCase.Breakdown_Location__Longitude__s);
                    
                    if(temp.Subject == 'Success') {
    
                        sirCase.Street__c = temp.Street__c;
                        sirCase.City__c = temp.City__c;
                        sirCase.Postal_Code__c = temp.Postal_Code__c;
                        
                        if(temp.Country__c == 'US' || temp.Country__c == 'CA' || temp.Country__c == 'Canada' || temp.Country__c == 'United States'){
                            sirCase.Country__c = temp.Country__c;
                            sirCase.Province__c = temp.Province__c;
                        }
                        else{
                            sirCase.Country__c = '';
                            sirCase.Province__c = '';
                        }
                        
                    }
                    
                    update sirCase;
                    
                    resp.resultCode = 'Success ' + sirCase.id + ' ' + temp.Subject;
                    
                }
                catch(Exception ex){
                
                    resp.resultCode = ex.getMessage();
                
                }
                
            }
            
        }
        else if(reqObj.referenceId != null){
        
            resp.resultCode = '200';
            
        }
        
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
        RestContext.response.responseBody = body;     
        
    }
    
    global class RequestService {
    
        public String referenceId {get; set;}
        public Location location {get; set;}
        public String serviceType {get;set;}
        
    } 
    global class Location {
    
        public Geopoint geopoint {get; set;}
        
    }
    global class Geopoint {
    
        public Decimal latitude {get; set;}
        public Decimal longitude {get; set;}
        
    }   
    global class ResponseService {
    
        public String referenceId {get; set;}
        public Decimal latitude {get; set;}  
        public Decimal longitude {get; set;}
        public String resultCode {get;set;}
        
    }       
}