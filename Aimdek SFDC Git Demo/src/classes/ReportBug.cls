public with sharing class ReportBug {
        Case thisCase {set;get;}
        string bug {set;get;}
        string descr {set;get;}

    public ReportBug(ApexPages.StandardController controller) {
        thisCase = (Case)controller.getRecord();
        bug = ApexPages.currentPage().getParameters().get('B');
        descr = ApexPages.currentPage().getParameters().get('D');
        system.debug('thisCase:' + thisCase.id);
        system.debug('bug option:' + bug);
        system.debug('descr:' + descr);
    }
    
    public PageReference SaveBug()
    {
    	Bug__c b = new Bug__c();
        try
        {
	        b.case__c= thisCase.Id;
	        b.type__c=bug;
	        b.description__c = descr;
	
	        insert b;
        }
        catch(System.exception ex)
        {
        	system.debug('ex:' + ex);
        }
        
        system.debug('bug:' + b);
        return new PageReference('/'+thisCase.Id).setRedirect(TRUE);
    }

}