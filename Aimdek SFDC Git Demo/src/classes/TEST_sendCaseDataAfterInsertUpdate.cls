@isTest(SeeAllData=true)
private class TEST_sendCaseDataAfterInsertUpdate{
    static Account club {get;set;}
    static Account client {get;set;}
    static Account tow {get;set;}
    static Contact cont {get;set;}
    static Territory_Coverage__c terr {get;set;}
    static Program__c prog {get;set;}
    static VIN__c vin {get;set;}
    static CA__c settings {get;set;}
    
    static void Utility()
    {
        club = new Account(Name='club CAA', Account_Number__c='999000',ETA__c= 60, RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        
        client = new Account(
            Name='Mercedes-Benz', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;
        
        cont = new Contact(LastName='Test',Email='abc@mail.com');
        insert cont;
        
        terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Florida',
            Province_Code__c='FL'
        );
        insert terr;
        
        prog =[select id,name,program_code__c from program__c where program_code__c = '960']; 
        
        vin = new VIN__c(
            Name='VINxxxxxxxxxxxxxxx',
            UID__c='VINxxxxxxxxxxxxxxx',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        settings = CA__c.getOrgDefaults();

    }
    static testMethod void myUnitTest1() {
        Utility();

        GlobalClass.firstRun = true;
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c='abc',
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Call_ID__c='ABC',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c ='Lost Keys',
            Miscellaneous_Call_Reason__c ='Lost Keys',
            Tow_Exchange__c ='Yes',
            Street__c='1000 AAA dr',
            PickupStreet__c = 'test',
            Destination_Name__c = 'test',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            Exchange_Pickup_Location_Email__c ='abcxyz@gmail.com;test1@mail.com',
            clientcasenumber__c = '1111111-abc',
            Program__c = prog.id
        );
        insert c;
        GlobalClass.firstRun = true;
        c.club__c = club.id;
        update c;
        GlobalClass.firstRun = true;
        c.Status =settings.ER_Status__c;
        update c;
    }
    static testMethod void myUnitTest2() {
        Utility();

        GlobalClass.firstRun = true;
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Call_ID__c='ABC',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c ='Lost Keys',
            Miscellaneous_Call_Reason__c ='Lost Keys',
            Tow_Exchange__c ='Yes',
            Street__c='1000 AAA dr',
            PickupStreet__c = 'test',
            Destination_Name__c = 'test',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            Exchange_Pickup_Location_Email__c ='abcxyz@gmail.com;test1@mail.com',
            clientcasenumber__c = '1111111-abc',
            Program__c = prog.id,
            Club__c = club.id
        );
        insert c;

        GlobalClass.firstRun = true;
        c.Status =settings.CL_Status__c;
        update c;
    }
}