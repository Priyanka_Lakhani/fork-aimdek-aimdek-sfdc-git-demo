public with sharing class EHIEmailDriveInExchange {
    private static list<string> getGPBREmails(string gpbr)
	{
	    list<string> emails = new list<string>();
        try
        {
            string bemail =  [select Tow_Exchange_Email_Address__c from account where gpbr__c = :gpbr and status__c != 'Inactive' limit 1].Tow_Exchange_Email_Address__c;
            string[] bemails = bemail.split(';');
            for (string st : bemails)
            {
            	if (st.contains('@'))
            	{
        			emails.add(st);
            	}
            }
        }
        catch(System.exception ex){}
		return emails;
	}
	
     @InvocableMethod(label='EmailDriveInExchange' description='EmailDriveInExchange')
    public static void EmailDriveInExchange(List<Case> cs)
    {
    	Case c = cs[0];

		try
		{    	
	        list<string> emails1 = getGPBREmails(c.gpbr__c);
	        list<string> emails2 = getGPBREmails(c.driveingpbr__c); //getNearestGPBREmail(c.Breakdown_Location__Latitude__s,c.Breakdown_Location__Longitude__s,getGPBRClientId(c.gpbr__c)); //getGPBREmails(c.VehRecoveryGPBR__c);
			string[] demails = CA__c.getOrgDefaults().EHIDriveInExchangeEmail__c.split(';');
        	for (string st : demails)
        		emails2.add(st);
	
			emails2.add(UserInfo.getUserEmail());
	
	 		Contact ctc = [select id, Email from Contact where email <> null limit 1];
	 		
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	
	        mail.setToAddresses(emails1);
	        mail.setCcAddresses(emails2);
	              
	
	    	List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
	    	
			mail.setTemplateID(CA__c.getOrgDefaults().Email_DriveinExchange__c);
		    mail.setWhatId(c.id);
			mail.setSaveAsActivity(true);
			mail.setTargetObjectId(ctc.id);
			//mail.setTargetObjectId('003m000000VMTyF'); //UserInfo.getUserId());
		    mail.setReplyTo('noreply@salesforce.com');
		    mail.SetSenderDisplayName ('Clubauto RSA LTD');
		    //mail.setSubject('Tow Exchange Vehicle Pickup');
		    mail.setUseSignature(false);
		    
		    lstMsgs.add(mail);
		    
		    system.debug('CA__c.getOrgDefaults().Email_Vehicle_Recovery__c:' + CA__c.getOrgDefaults().Email_DriveinExchange__c);
		    system.debug('c.id:' + c.id);
		            
		            
		    Savepoint sp = Database.setSavepoint();
		    Messaging.sendEmail(lstMsgs);
			Database.rollback(sp);
			//have to do trick with email template. 
			//it has to set targetobjectid as a contact id, then case inforation case be merged with template.
	
			List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
			for (Messaging.SingleEmailMessage email : lstMsgs) {
				Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
				emailToSend.setToAddresses(email.getToAddresses());
				emailToSend.setCcAddresses(email.getCcAddresses());
				emailToSend.setPlainTextBody(email.getPlainTextBody());
				emailToSend.setHTMLBody(email.getHTMLBody());
				emailToSend.setSubject(email.getSubject());
				lstMsgsToSend.add(emailToSend);
			}
			
			Messaging.sendEmail(lstMsgsToSend);
		}
		catch(System.exception ex){}
    }         
  
	
}