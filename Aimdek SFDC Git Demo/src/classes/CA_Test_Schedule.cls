@isTest private class CA_Test_Schedule {
    
    static testMethod void testService() {
        
        CA__c settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';
        upsert settings;
        
		Service_Tracker__c tracker = Service_Tracker__c.getOrgDefaults();
		tracker.ApplicationID__c='A';
		tracker.ApplicationPassword__c='A';
		tracker.ApplicationMethodName__c='A';
		tracker.End_Point__c='A';
		upsert tracker;
		
        Account club = new Account(Name='Club', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        update club;
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        Account tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;
        
        Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active'
        );
        insert prog;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        Case c = new Case(
            Status='Dispatched',
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Phone__c='123',
            Call_ID__c='ABC'
        );
        insert c;
        
		Integration__c i = new Integration__c(
			Case__c=c.Id,
			Parameters__c='test'
		);
		insert i;
		database.executebatch(new ReprocessIntegration('Select Id, Parameters__c from Integration__c where Case__c=\''+c.Id+'\'', Date.today().addDays(-100), Date.today().addDays(100), null),1);
		
        CloneCase cloneC = new CloneCase(new ApexPages.StandardController(c));
        cloneC.init();
        
        BatchUpdateVIN b = new BatchUpdateVIN('SELECT Id, PIN__c, Year__c, Make__c, Model__c, License__c, Plan_Name__c, First_Name__c, Last_Name__c, Personal_First_Name__c, Personal_Last_Name__c, Name, Program__r.Call_Flow_Autocomplete_Type__c FROM VIN__c ORDER BY Program__c ASC LIMIT 1'); 
        database.executebatch(b,2000);
		
		ServiceTrackerBatchable stb = new ServiceTrackerBatchable('SELECT Id, Club_Code__c, VIN_Member_ID__c FROM Case where Id=\''+c.Id+'\''); 
        try { database.executebatch(stb,1); } catch(Exception e) {}
        
        System.schedule('S1', '0 0 0 3 9 ? 2022', new Schedule_BatchUpdateVIN());
        System.schedule('S2', '0 0 0 3 9 ? 2022', new RefreshDispatchClubs());
        System.schedule('S3', '0 0 0 3 9 ? 2022', new RefreshLastCaseNumber());
		try { System.schedule('S4', '0 0 0 3 9 ? 2022', new Schedule_ServiceTracker()); } catch(Exception e) {}
    }
}