public class TestDrivingDistanceController{

    public String distance { get; set; }

    public String toLong { get; set; }

    public String toLat { get; set; }

    public String fromLong { get; set; }

    public String fromLat { get; set; }
    
    public void calculateDrivingDistance() {
        String fromLatLong = '';
        String toLatLong = '';
        decimal distanceCoverage = 0;
        try{
            fromLatLong = EncodingUtil.urlEncode(
                fromLat+ ', '+fromLong,
                'UTF-8');
             system.debug(fromLatLong );
            toLatLong = EncodingUtil.urlEncode(
                toLat+ ', '+toLong,
                'UTF-8');
                system.debug(toLatLong);
            googleMaps gm = new googleMaps(fromLatLong ,toLatLong);
            distanceCoverage = gm.distanceInMiles;
            System.debug(distanceCoverage);
            distance = String.valueOf(distanceCoverage); 
         }catch(System.Exception ex){ 
              System.debug('Distance calculation Exception:'+ex);
         }
    }

}