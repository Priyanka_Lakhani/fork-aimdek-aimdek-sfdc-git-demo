@isTest
private class SurveyController_Test {
    static Account client{get;set;}
    static Account dealer{get;set;}

    static VIN__c vin{get;set;}
    static Program__c prog{get;set;}
    static Case c {get;set;}

    static VIN__c vin2{get;set;}
    static Program__c prog2{get;set;}

    static VIN__c vin3{get;set;}
    static Program__c prog3{get;set;}

    static VIN__c vin4{get;set;}
    static Program__c prog4{get;set;}

    static VIN__c vin5{get;set;}
    static Program__c prog5{get;set;}

    static Survey__c survey {get;set;}
  static CA__c settings {get;set;}
  static Survey_Question__c sq1 {get;set;}
  static Survey_Question__c sq2 {get;set;}
    static SurveyResponse__c sr1 {get;set;}
    static SurveyResponse__c sr2 {get;set;}

  //static Sites__c sites {get;set;}
  
    static void Utility()
    {
      Sites__c sites = new Sites__c();
      sites.name = 'Survey';
      sites.value__c = 'http://www.salesforce.com';
      upsert sites;
      
      Sites__c sites1 = new Sites__c();
      sites1.name = 'JLRLogo';
      sites1.value__c = '015m0000001BLhX';
      upsert sites1;
      
      Sites__c sites2 = new Sites__c();
      sites2.name = 'Instance';
      sites2.value__c = 'cs20';
      upsert sites2;
      
      
      
      settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';
        upsert settings;
        
         client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
      survey = new Survey__c(
        name ='JLR Survey'
      );
      insert survey;
      
      
      sq1 = new Survey_Question__c();
      sq1.question__c = 'how are your?';
      sq1.order__c = 1;
      sq1.survey__c = survey.id;
      sq1.type__c = 'Single Select';
      upsert sq1;

      sq2 = new Survey_Question__c();
      sq2.question__c = 'how old are you?';
      sq2.order__c = 2;
      sq2.survey__c = survey.id;
      sq2.type__c = 'Single Select';
      upsert sq2;
      
        prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='858',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        insert prog;

        prog2 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='560',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        insert prog2;

        prog3 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='563',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        insert prog3;

        prog4 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='566',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        insert prog4;

        prog5 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='569',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        insert prog5;
                
         vin = new VIN__c(
            Name='VIN1234567890456',
            UID__c='VIN1234567890456',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vin;

        vin2 = new VIN__c(
            Name='VIN123456789045',
            UID__c='VIN123456789045',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog2.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vin2;

        vin3 = new VIN__c(
            Name='VIN12345678904560',
            UID__c='VIN12345678904560',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog3.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vin3;

        vin4 = new VIN__c(
            Name='VIN12345678904563',
            UID__c='VIN12345678904537',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog4.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vin4;

        vin5 = new VIN__c(
            Name='VIN12345678104567',
            UID__c='VIN12345678901567',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog5.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vin5;


    }


    static testMethod void myUnitTest1() {
       
       Utility();
       c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = DateTime.newInstance(2015,10,5,9,0,0),
          Club_Call_Number__c = '2251',
          program__c = prog.id,
          Vehicle_Make__c ='Jaguar',
          first_name__c='test',
          last_name__c='test',
          email__c = 'abc@clubautoltd.com',
          Country__c = 'US'
        );
        insert c;


    c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c from case where id =:c.id];
        
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c).replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss;
        update c;
    
      system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        SurveyController sc = new SurveyController();
        sc.init();    
        
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-550'); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
    
      system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
       sc = new SurveyController();
        sc.init();    
        
        sr1 = new SurveyResponse__c();
        sr1.Case__c = c.id;
        sr1.Phone__c = '123456';
        sr1.Response__c = '1';
        sr1.ResponseDate__c = DateTime.now();
        sr1.SubmissionDate__c = DateTime.now();
        sr1.SurveyQuestion__c = sq1.id;
        sr1.Callback__c = 'Yes';
        insert sr1;

        sr2 = new SurveyResponse__c();
        sr2.Case__c = c.id;
        sr2.Phone__c = '123456';
        sr2.Response__c = '1';
        sr2.ResponseDate__c = DateTime.now();
        sr2.SubmissionDate__c = DateTime.now();
        sr2.SurveyQuestion__c = sq2.id;
        sr2.Callback__c = 'Yes';
        insert sr2;

        
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
    
      system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        sc.init();    
        sc.submitResults();

        
        sc = new SurveyController();
        sc.init(); 
    
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-555'); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c+'-xx'); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
    
      system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        sc.init();           
        
        /*
        ApexPages.currentPage().getParameters().put('id','xxxxx');
        sc = new SurveyController();
        sc.init();*/           

    }
    
  static testMethod void myUnitTest2() 
  {
         
       Utility();
       c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin2.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = DateTime.newInstance(2015,10,5,9,0,0),
          Club_Call_Number__c = '2251',
          program__c = prog2.id,
          Vehicle_Make__c ='Jaguar',
          first_name__c='test',
          last_name__c='test',
          email__c = 'abc@clubautoltd.com',
          Country__c = 'US'
        );
        insert c;
    
    c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
      system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();    
  }
    
    static testMethod void myUnitTest3() 
    {
       Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin3.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = DateTime.newInstance(2015,10,5,9,0,0),
          Club_Call_Number__c = '2251',
          program__c = prog3.id,
          Vehicle_Make__c ='Jaguar',
          first_name__c='test',
          last_name__c='test',
          email__c = 'abc@clubautoltd.com',
          Country__c = 'US'
        );
        insert c;
    
    c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,aa,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();    
        
        ss1 = GlobalClass.ScrambleText('aabc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();   
        
        ss1 = GlobalClass.ScrambleText('a@abc.com,007'); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();    
        
        ApexPages.currentPage().getParameters().put('unsubscribe','xxxx');
        sc = new SurveyController();
        sc.init();    

    }

    static testMethod void myUnitTest4() 
    {
        Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin4.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = DateTime.newInstance(2015,10,5,9,0,0),
            Club_Call_Number__c = '2251',
            program__c = prog4.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US'
        );
        insert c;
        
        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,aa,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();    
        
        ss1 = GlobalClass.ScrambleText('aabc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();   
        
        ss1 = GlobalClass.ScrambleText('a@abc.com,007'); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();    
        
        ApexPages.currentPage().getParameters().put('unsubscribe','xxxx');
        sc = new SurveyController();
        sc.init();    
    }

    static testMethod void myUnitTest5() 
    {
        Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin5.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = DateTime.newInstance(2015,10,5,9,0,0),
            Club_Call_Number__c = '2251',
            program__c = prog5.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US'
        );
        insert c;
        
        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,aa,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();  
        sc.getAdditValue();  
        
        ss1 = GlobalClass.ScrambleText('aabc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();   
        
        ss1 = GlobalClass.ScrambleText('a@abc.com,007'); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();    
        
        ApexPages.currentPage().getParameters().put('unsubscribe','xxxx');
        sc = new SurveyController();
        sc.init();
        //sc.additionalQ();
        sc.clearAddInfoPhone();  
        //sc.submitResults();  
    }
}