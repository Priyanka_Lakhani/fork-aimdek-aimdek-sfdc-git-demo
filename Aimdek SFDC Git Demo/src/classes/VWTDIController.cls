public with sharing class VWTDIController {
    
    public boolean authenticated {get;set;}
    public string sessionId {get;set;}
    //public string loginId {get;set;}
    public string firstName {get;set;}
    public string lastName {get;set;}
    public string VIN {get;set;}
    public string phone {get;set;}
    public string email {get;set;}
    public string callReason {get;set;}
    
    public string Street {get;set;}
    public string City {get;set;}
    public string Province {get;set;}
    //public string Country {get;set;}
    public string PostalCode {get;set;}


    public string year {get;set;}
    public string make {get;set;}
    public string model {get;set;}
    
    public string prgname {get;set;}
    
    public string logourl {get;set;}
    public boolean validvin {get;set;}
    //public string msg {get;set;}
    public string comments {get;set;}
    
    public list<SelectOption> reasons {get;set;}
    public string loginname {get;set;}
    public string pwd {get;set;}
    public string newpwd1 {get;set;}
    public string newpwd2 {get;set;}
    public boolean reset {get;set;}
	public string LoginUser {get;set;}
	private string loginUserId;
	private FormUser__c usr {get;set;}
	private string IP;
	private id prgId;
	private id vinId;
	public boolean isSubmitted {get;set;}
	private string whitelist;
	private string loginIP;
	public string loginfirstname {get;set;}
	public string space {get;set;}
	public boolean internalUser {get;set;}
	
	public string casenumber {get;set;}
	public boolean isFrench {get;set;}
	
    public void Submit()
    {
		if (!validateIP())
		{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Operation Failed'));
    		authenticated = false;
    		return;
		}
    	string s1 = validateFields();
    	
    	if (s1 != '')
    	{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, s1));
    		return;
    	}
    	
    	casenumber='';
    	
    	try
    	{
    		Case c = new Case();
	    	c.program__c = prgId;
	    	c.first_name__c = firstname;
	    	c.last_name__c = lastname;
	    	c.VIN_Member_ID__c = vin;
	    	c.vin__c = vinId;
	    	c.phone__c = phone;
	    	c.email__c = email;
	    	//c.Home_Street__c = street;
	    	//c.home_city__c = city;
	    	//c.Home_Province__c = province;
	    	//c.home_country__c = 'Canada';
	    	//c.Home_Postal_Code__c = postalcode;
	    	c.Log_A_Call__c = comments;
	    	c.TDIClaimCallReason__c = callReason;
	    	if (!internalUser)
	    		c.FormUser__c = loginUserId;
	    	c.Type='Recall';
	    	c.Status = 'Closed (Call Cleared)';
	    	c.CTI_Term__c = 'VWTDIClaims';
	    	upsert c;
	    	
	    	
	    	//validvin = false;
	    	vin = '';
	    	year='';
	    	model='';
	    	make='';
	    	firstname ='';
	    	lastname='';
	    	phone='';
	    	email='';
	    	comments ='';
	    	callReason='';
	    	isSubmitted=true;
	    	
	    	casenumber = [select casenumber from case where id = :c.id].casenumber;
	    	system.debug('casenumber:' + c.casenumber);

    	}
    	catch(System.exception ex)
    	{
    	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Submittion Failure.' + ex.getmessage()));
    	}
    }
    
    public void Login()
    {
    	//select email__c,firstname__c,lastname__c,loginname__c,password__c,reset__c,sessioncode__c from FormUser__c
    	
    	//select LoginIP__c,LoginTime__c,TDI__c,result__c,loginName__c,session__c from FormUserHistory__c
    
        Map<String, String>  headers = ApexPages.currentPage().getHeaders();
		loginIP=headers.get('X-Salesforce-SIP');   	
            //for(String s : headers.keySet()) {
             //   System.debug('Header (' + s + '): ' + headers.get(s));}
                
		
		loginUserId =null;
		//prgId=null;
    	FormUserHistory__c userlogin = new FormUserHistory__c();
    	userlogin.LoginTime__c = System.now();
    	userLogin.loginIp__c = IP;	
    	userLogin.LoginName__c = loginname;
    	try
    	{
    		
    		pwd = string.valueof(pwd.hashcode());
    		//system.debug('pwd:' + pwd);
    		//system.debug('username:' + loginname);
    		usr =[select id,email__c,firstname__c,lastname__c,loginname__c,password__c,reset__c,sessioncode__c,whitelist__c from FormUser__c where loginname__c = :loginname and password__c =:pwd limit 1];
    		whitelist = usr.whitelist__c;
    		
    		if (!validateIP())
    		{
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Login Failed'));
	    		authenticated = false;
	    		userlogin.result__c = 'Login Failure';
    		}
    		else
    		{
	    		LoginUser = usr.firstname__c +' ' + usr.lastname__c;
	    		loginfirstname = usr.firstname__c;
	    		reset = usr.reset__c;
				
				if (!reset)
				{
					sessionid = GlobalClass.NewGUID();
					usr.sessioncode__c = sessionid;
					update usr;
					authenticated = true;
					
					loginUserId = usr.id;	
				}    		
	    		
	    		userLogin.TDI__c = usr.id;
	    		userlogin.result__c = 'Login';
	    		userlogin.session__c = sessionid;
    		}
    	}
    	catch(System.exception ex)
    	{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Login Failed'));
    		
    		authenticated = false;
    		userlogin.result__c = 'Login Failure';
    	}
    	
    	upsert userLogin;
    	
    }
    
    public void ResetPwd()
    {
    	reset= true;
    	authenticated=false;
    }
    
    public void ChangePwd()
    {
		if (!validateIP())
		{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Operation Failed'));
    		authenticated = false;
    		return;
		}
    	
    	if (newpwd1 != newpwd2)
    	{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'New Password not matching'));
    		return;
    	}
    	
    	try
    	{
    		pwd = string.valueof(pwd.hashcode());
			usr =[select id,email__c,firstname__c,lastname__c,loginname__c,password__c,reset__c,sessioncode__c from FormUser__c where loginname__c = :loginname and password__c =:pwd limit 1];
    		
    		usr.password__c = string.valueof(newpwd1.hashcode());
    		usr.reset__c  = false;
    		update usr;
    		loginname = '';
    		reset = false;
    		
    	}
    	catch(System.exception ex)
    	{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Change password failed'));
    	}
    	
    }
    
    public void Logout()
    {
		FormUserHistory__c userlogin = new FormUserHistory__c();
		userlogin.LoginTime__c = System.now();
		userLogin.loginIp__c = IP;	
		userLogin.LoginName__c = loginname;
		userLogin.Result__c = 'Logout';
   		userlogin.session__c = sessionid;
		upsert userlogin;
		
		authenticated = false;
		//validvin = false;   	
		loginUser=null;
		loginName='';
    }
    
    public void Next()
    {
    	isSubmitted=false;
    }
    
    public void checkVIN()
    {
    	
		if (!validateIP())
		{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Operation Failed'));
    		authenticated = false;
    		return;
		}
    	
    	firstname='';
    	lastname='';
    	phone='';
    	email='';
    	street ='';
    	city='';
    	province='';
    	postalcode='';
    	year='';
    	model='';
    	make='';

    	comments = '';
    	vinId=null;
    	isSubmitted = false;
    	
    	try
    	{
    		vin__c v = [select id,name,first_name__c,last_name__c,phone__c,email__c,street__c,city__c,State__c,Postal_Code__c,year__c,model__c,make__c,Program__r.name from vin__c where name =:vin and program__r.program_code__c in ('511','512')];
    		vinId = v.Id;
    		firstname = v.first_name__c;
    		lastname =v.last_name__c;
    		phone = v.phone__c;
    		email = v.email__c;
    		street = v.street__c;
    		city = v.city__c;
    		province=v.State__c;
    		postalcode = v.postal_code__c;
    		year=v.year__c;
    		make=v.make__c;
    		model=v.model__c;
    		prgname = v.program__r.name;
	    	//validvin = true;
    		
    	}
    	catch(System.exception ex){    	
    		//validvin = false; 
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'VIN is Not Found. Please get the VIN from vehicle permit or car insurance paper.'));
    		
    	}
    	//validvin = true;
    	
    }
    public VWTDIController()
    {
    	internaluser = false;
    	isSubmitted = false;
    	validvin = true;
    	authenticated = false;
    	reset = false;
    	string orgid =[SELECT id, Name FROM Organization limit 1].id;
       	String instance = Sites__c.getAll().get('Instance').Value__c;
   		logourl = 'https://c.' + instance + '.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=' + Sites__c.getAll().get('VWLogo').Value__c + '&oid=' + orgid;
    
		prgId = [select id from program__c where program_code__c ='888' limit 1].id;	
    
    	reasons = new list<SelectOption>();
    	
    	reasons.add(new selectoption('None','-- None --'));
    	reasons.add(new selectoption('FAQs','FAQs'));
    	reasons.add(new selectoption('Claims Portal – General Inquiry','Claims Portal – General Inquiry'));
   		reasons.add(new selectoption('Settlement Website – General Inquiry','Settlement Website – General Inquiry'));
   		reasons.add(new selectoption('Settlement Inquiries – Transfer to RP','Settlement Inquiries – Transfer to RP'));
   		reasons.add(new selectoption('Eligibility – Transfer to RP','Eligibility – Transfer to RP'));
   		reasons.add(new selectoption('Transfer to VW CC','Transfer to VW CC'));
   		reasons.add(new selectoption('Escalation – Transfer to RP','Escalation – Transfer to RP'));
   		reasons.add(new selectoption('Others','Others'));
    	Map<String, String>  headers = ApexPages.currentPage().getHeaders();
		IP=headers.get('X-Salesforce-SIP');
		space = '                               ';   	

		isFrench = false;
		try
		{
			string uRoleId = UserInfo.getUserRoleId();
			UserRole ur = [select id,name,portalaccountid,portaltype from userrole where id=:uRoleId];
    		LoginUser = UserInfo.GetFirstName() + ' ' + UserInfo.GetLastName();
    		loginfirstname = UserInfo.GetFirstName();
    		loginUserId = UserInfo.GetUserName();
			authenticated = true;
			internaluser = true;
		}
		catch(system.exception ex)
		{
		}
			
    }
    
    private boolean validateIP()
    {
    	if (internaluser)
    		return true;
    		
    	if (IP != loginIP)
    		return false;
    		
    	boolean rtn = false;
    	Map<String, String>  headers = ApexPages.currentPage().getHeaders();
		IP=headers.get('X-Salesforce-SIP');
		
            for(String s : headers.keySet()) {
                System.debug('Header (' + s + '): ' + headers.get(s));}
		
		if (whitelist==null || whitelist == '')
		{
			rtn = true;
		}
		else
		{
			string[] ips = whitelist.split(';');
			for (string s:ips)
			{
				if (s== IP)
				{
					rtn = true;
					break;
				}
			}
		}
		return rtn;
    }
    
    private string validateFields()
    {
    	string rtn = '';
    	if ( firstname==null || firstname == '')
    	{
    		rtn += 'First Name cannot be blank.\r\n';
    	}
    	if ( lastname==null || lastname == '')
    		rtn += 'Last Name cannot be blank.\r\n';
    	
    	if ( phone==null || phone == '')
			rtn += 'Phone cannot be blank.\r\n';
	
    	if ( callReason==null || callReason == 'None')
			rtn += 'Call Reason cannot be blank.\r\n';
    	
    	return rtn;
    }
    
    public void ChangeLanguage()
    {
    	isFrench = !isFrench;
    }
}