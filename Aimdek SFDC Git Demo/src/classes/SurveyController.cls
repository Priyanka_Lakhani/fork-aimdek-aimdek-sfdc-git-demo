public with sharing class SurveyController {
    
    public string logourl {get;set;}
    //public string firstname {get;set;}
    public boolean validsurvey {get;set;}
    public boolean thankyou {get;set;}
    public boolean isUnSubed {get;set;}
    public string unSubemail {get;set;}
    public string title {get;set;}
    public string errorMsg {get;set;}

    public List<SelectOption> getAdditValue() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }  
    public string additInfo {get;set;}
    public string additPhone {get;set;}
    public string additInfoText {get;set;}
    public string additPhoneNumber {get;set;}
    public boolean allQuestions {get;set;}
    public boolean oneTwoScore {get;set;}
    public boolean additInfoErr {get;set;}
    public boolean phoneNumErr {get;set;}

    public list<SFQuestion> questions {get;set;}
    
    private list<SurveyResponse__c> responses;
    
    public void init()
    {
        additInfoErr = false;
        phoneNumErr = false;
        allQuestions = false;
        oneTwoScore = false;
        validsurvey = false;
        isUnSubed=false;
        
        string orgid =[SELECT id, Name FROM Organization limit 1].id;
        String instance = Sites__c.getAll().get('Instance').Value__c;
        
        string clid ='';
        
        //logo in document object
    
        string pid = ApexPages.currentPage().getParameters().get('id');
        if (pid !=null)     
            pid = string.escapeSingleQuotes(pid);
        
        string unsub = ApexPages.currentPage().getParameters().get('unsubscribe');
        if (unsub !=null)
        {
            unsub = string.escapeSingleQuotes(unsub);
    
            try
            {
                unsub=globalclass.UnscrambleText(unsub); 
                
                string[] demail = unsub.split(',');
                if (demail.size() != 2)
                    return;
                unsub = demail[0];
                clid = demail[1];
                
                if (clid == '858' || clid == '858' || clid == '859' || clid == '860' || clid == '861' || clid == '862' || clid == '863' || clid == '864' || 
                    clid == '865' || clid == '866' || clid == '867')
                {
                    //logourl = 'https://c.' + instance + '.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=015m0000001BLhX&oid=00Dm00000004gir';
                    logourl = 'https://c.' + instance + '.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=' + Sites__c.getAll().get('JLRLogo').Value__c + '&oid=' + orgid;
                    title = 'Jaguar Land Rover Roadside Assistance Survey';
                }
                else if (clid == '560' || clid == '561' || clid == '562' || clid == '563' || clid == '564' || clid == '565' || clid == '566' || clid == '567' || 
                         clid == '568' || clid == '569') {
                    if (clid == '560' || clid == '561' || clid == '562') {
                        title = 'Honda Roadside Assistance Survey';
                    }
                    if (clid == '563' || clid == '564' || clid == '565') {
                        title = 'Acura Roadside Assistance Survey';
                    }
                    if (clid == '566' || clid == '567' || 
                         clid == '568') {
                        title = 'Acura NSX Roadside Assistance Survey';
                    }
                    if (clid == '569') {
                        title = 'Honda Motorcycle Roadside Assistance Survey';
                    }
                }
                else
                    return;
                
            }catch(System.exception ex){return;}    
            
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; 
            // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
            Pattern MyPattern = Pattern.compile(emailRegex);
            Matcher MyMatcher = MyPattern.matcher(unsub);

            if (!MyMatcher.matches()) 
                return; 
            system.debug('1111111111111 unsub: ' + unsub);
            if (unsub!='')
            {
                SurveyUnsubscriber__c su = new SurveyUnsubscriber__c();
                su.email__c = unsub;
                upsert su;
                unSubemail = unsub;
                isUnSubed = true;
            }
            return;
        }
        
        //string tempid = pid.replace('%2B','+');
        string upid='';
        system.debug('pid:' + pid);
        pid = pid.replace('%2B','+').replace('%2F','/');
        system.debug('pid:' + pid);
        try
        {
            upid=globalclass.UnscrambleText(pid); 
        }catch(System.exception ex){return;}    
        
        string[] cids = upid.split('-');
        if (cids.size() != 3)
            return;
            
        string cid = cids[1];
        
        string cdate = cids[0];
        
        clid = cids[2];
        
        system.debug('cdate:' + cdate);
        
        if (clid == '858' || clid == '858' || clid == '859' || clid == '860' || clid == '861' || clid == '862' || clid == '863' || clid == '864' || clid == '865' || clid == '866' || clid == '867')
        {
            //logourl = 'https://c.' + instance + '.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=015m0000001BLhX&oid=00Dm00000004gir';
            logourl = 'https://c.' + instance + '.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=' + Sites__c.getAll().get('JLRLogo').Value__c + '&oid=' + orgid;
            title = 'Jaguar Land Rover Roadside Assistance Survey';
        }
        else if (clid == '560' || clid == '561' || clid == '562' || clid == '563' || clid == '564' || clid == '565' || clid == '566' || clid == '567' || 
                         clid == '568' || clid == '569') {
            if (clid == '560' || clid == '561' || clid == '562') {
                title = 'Honda Roadside Assistance Survey';
            }
            if (clid == '563' || clid == '564' || clid == '565') {
                title = 'Acura Roadside Assistance Survey';
            }
            if (clid == '566' || clid == '567' || 
                 clid == '568') {
                title = 'Acura NSX Roadside Assistance Survey';
            }
            if (clid == '569') {
                title = 'Honda Motorcycle Roadside Assistance Survey';
            }
        }
        else
            return;
        
        try
        {
            case c = [select id,createddate,casenumber,first_name__c,last_name__c, email__c,Vehicle_Make__c,Vehicle_Model__c,Trouble_Code__c,surveysent__c,program__r.survey__r.name,program__r.survey__c  from case where SurveySent__c != null and program__r.survey__c !=null and casenumber = :cid limit 1]; //
            
            system.debug('c.createddate:' + c.createddate);
            //if (cdate != GlobalClass.getDatayyyyMMdd(c.Createddate))
            //  return;
            //if (c.createddate.adddays(1) < system.Date.today())
            //return;
            system.debug('pid:' + pid);
            system.debug('c.surveysent:' + c.surveysent__c);
            
            if (pid.replace('+','%2B').replace('/','%2F') != c.surveysent__c)
            return;
            
            responses = new list<SurveyResponse__c>();
            
            questions = new list<SFQuestion>();
            validsurvey = true;
            thankyou = false;
            //firstname = c.first_name__c;
            
            list<Survey_Question__c> surveyQs = [select id,order__c,Question__c,Required__c,Type__c from Survey_Question__c where Survey__c = :c.program__r.survey__c order by order__c];
            list<id> sqids = new list<id>();
            for (Survey_Question__c q : surveyQs)
            {
                sqids.add(q.id);
            }
            
            boolean submittedbefore = false;
            
            responses = [select id,ResponseDate__c,Case__c,Response__c,SubmissionDate__c,SurveyQuestion__c,Phone__c,Additional_Information__c,Callback__c from SurveyResponse__c where case__c =:c.id and SurveyQuestion__c in :sqids];
            system.debug('responses.size : ' + responses.size());
            
            if (responses.size() > 0)
            {
                submittedbefore = true;     
                validsurvey = false;
            }

            for (Survey_Question__c q : surveyQs)
            {
                system.debug(q);
    
                SFQuestion sfq = new SFQuestion();
                sfq.qid = q.id;
                sfq.singleOptions = new list<SelectOption>();
                sfq.question = q.Question__c;
                sfq.qType = q.type__c;
                if (q.type__c == 'Single Select')
                {
                    list<SelectOption> selops = new List<SelectOption>();
                    for (integer i=1;i<11;i++)
                    {
                        selops.add(new SelectOption(string.valueof(i),string.valueof(i)));
                    }
                    sfq.singleOptions = selops;
                }
                sfq.orderNum = string.valueof(q.order__c);
                sfq.renderSelectRadio = true;
                sfq.required = q.required__c;
                if (submittedbefore)
                {
                    for (SurveyResponse__c rp:   responses) 
                    {
                        if (rp.SurveyQuestion__c == q.id)
                        {
                            sfq.selectedoption = rp.Response__c;
                            break;
                        }
                    }
                }
                questions.add(sfq);
                
                SurveyResponse__c rsp = new SurveyResponse__c();
                rsp.SurveyQuestion__c = q.id;
                rsp.case__c = c.id;
                rsp.ResponseDate__c = System.Datetime.now();
                responses.add(rsp);
            }
                
        system.debug('reponses1:' + responses);
        }
        catch(System.exception ex){
        //firstname = ex.getMessage();
        system.debug('error:' + ex.getMessage());
        }
    }
    
    public void submitResults()
    {
        errorMsg = '';
        additInfoErr = false;
        phoneNumErr = false;
        boolean iserr = false;
        for (SFQuestion question : questions )
        {
            question.errorMsg = '';
            if (question.selectedOption == null)
            {
                question.errorMsg = '***';
                iserr = true;
            }
        }

        if (iserr)
        {
            errorMsg = 'Please make sure all the questions are answered';
            return;     
        }

        if(oneTwoScore && allQuestions){
            if(additInfo == '' || additInfo == null){
                additInfoErr = true;
                errorMsg = 'Please make sure all the questions are answered';
                return;
            }
            else{
                if(additInfo == 'Yes'){
                    if(additInfoText == '' || additInfoText == null){
                        additInfoErr = true;
                        errorMsg = 'Please make sure all the questions are answered';
                        return;
                    }
                    if(additPhone == '' || additPhone == null){
                        phoneNumErr = true;
                        errorMsg = 'Please make sure all the questions are answered';
                        return;
                    }
                    else{
                        if(additPhone == 'Yes'){
                            if(additPhoneNumber == '' || additPhoneNumber == null){
                                phoneNumErr = true;
                                errorMsg = 'Please make sure all the questions are answered';
                                return;
                            }
                        }
                    }
                }
            }
        }
        
        for (SurveyResponse__c response : responses)
        {
            response.SubmissionDate__c = system.datetime.now();
            for (SFQuestion question : questions )
            {
                if (response.SurveyQuestion__c == question.qid)
                {
                    response.Response__c = question.selectedOption;
                }
            }
            if(response.Response__c == '1' || response.Response__c == '2'){
                response.Additional_Information__c = additInfoText;
                response.Callback__c = additPhone;
                response.Phone__c = additPhoneNumber;
            }
        }
        system.debug('reponses1:' + responses);
        upsert responses;
        thankyou = true;
    }

    public void additionalQ(){
        allQuestions = true;
        oneTwoScore = false;
        additInfo = '';
        additInfoText = '';
        additPhone = '';
        additPhoneNumber = '';
        for(SFQuestion q : Questions){
            if(q.selectedOption == '' || q.selectedOption == null){
                allQuestions = false;
            }
            if(q.selectedOption == '1' || q.selectedOption == '2'){
                
                oneTwoScore = true;
            }
        }
    }

    public void clearAddInfoPhone(){
        if(additInfo == 'No'){
            additInfoText = '';
        }
        if(additPhone == 'No'){
            additPhoneNumber = '';
        }
    }
    
    public class SFQuestion
    {
        public id qid {get;set;}
        public string question {get;set;}
        public string qType {get;set;}
        public List<SelectOption> singleOptions{get; set;}
        public String selectedOption {get;set;}
        public boolean renderSelectRadio {get;set;}
        public string orderNum {get;set;} 
        public boolean required {get;set;}
        public string errorMsg {get;set;}
    }
}