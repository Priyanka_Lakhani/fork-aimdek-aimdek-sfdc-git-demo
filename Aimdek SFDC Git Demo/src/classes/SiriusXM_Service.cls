public class SiriusXM_Service{

    public class ReadDetailData_Request{

        public String referenceId{get;set;}
        public String contactLastName {get;set;}
        public String contactPhoneNumber {get;set;}

    }

//===========ReadDetailData
    public static ReadDetailData_Output readDetailData(String refId, String conLname, String conPhone, String programType){

        try{

            Sirius_XM_Service__c settings = Sirius_XM_Service__c.getOrgDefaults();
            String endpoint = settings.End_Point__c + '/' + programType.toLowerCase() + '/ReadDetailData';

            ReadDetailData_Request sir_req = new ReadDetailData_Request();
            sir_req.referenceId = refId;
            sir_req.contactLastName  = conLname;
            sir_req.contactPhoneNumber = conphone;
            
            String param = JSON.serialize(sir_req);
            System.debug('JSON: ' + param);
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint);
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            req.setBody(param); 
            
            Http http = new Http();
            String outputJson;
            if (!Test.isRunningTest()) {
            
                HTTPResponse res = http.send(req);
                System.debug('THE RESPONSE: ' + res.getBody());
                outputJson = res.getBody();
                
            }
            else {
                outputJson = '{"resultCode":"NO_ERROR","accountStatus":"Active","referenceId": "123456","callingParty": "String","eventStatus": "CREATED","deviceEventStatus": "CREATED","location": {' +
                        '"dateTime": 1008581447000,"geopoint": {"altitude": {"value": 0.0, "unit": "FEET"},"latitude": 108823999,"longitude": -352191263},' +
                        '"heading": 38,"lastGoodGps": 71,"speed": {"value": 50,"unit": "MPH"},"horizontalDop": 1,"positionDop": 1,"timeDop": 0,"verticalDop": 0},' +
                        '"serviceType": "CONCIERGE","vehicleInfo": {"color": "red","licensePlate": {"license": "String","region": "String","country": "USA"},' +
                        '"vin": "JTHBK1GG2G2232768","modelMake": "LEXUS","modelName": "LS 460","modelType": "String","modelYear": "2017"},' + 
                        '"ownerInfo": {"name":{"firstName":"test","lastName":"test"},"phones":[{"phoneType":"MOBILE","phoneNumber":"123345"},{"phoneType":"HOME","phoneNumber":"123345"}],"emails":[{"emailAddress":"test"}],"addresses":[{"addressType":"mailing"},{"addressType":"TEST"}]}}';
            }
            
            
            System.debug('RESPONSE JSON 2 APEX ' + System.JSON.deserialize(outputJson, ReadDetailData_Output.class));
            ReadDetailData_Output sir_output = (ReadDetailData_Output) JSON.deserialize(outputJson, ReadDetailData_Output.class);

            return sir_output;

        }
        catch(Exception ex){

            System.debug('TB SIRIUS XM ReadDetailData: ' + ex.getMessage());
            return null;

        }

    }
    
    public class ReadDetailData_Output{

        public String accountStatus {get;set;}
        public VehicleType vehicleInfo {get;set;}
        public ContactType ownerInfo {get;set;}
        public List<ContactType> contactInfo {get;set;}
        public String resultCode {get;set;}
        public String referenceId {get;set;}

        public ReadDetailData_Output(){

            vehicleInfo = new VehicleType();
            ownerInfo = new ContactType();
            contactInfo = new List<ContactType>();

        }

    }
    
    public class VehicleType{

        public String color {get;set;}
        //public String licensePlate {get;set;}
        public String vin {get;set;}
        public String modelMake {get;set;}
        public String modelName {get;set;}
        public String modelYear {get;set;}

    }
    
    public class ContactType{

        public List<ContactAddressType> addresses {get;set;}
        public String companyName {get;set;}
        public List<EmailAddressType> emails {get;set;}
        public NameType name {get;set;}
        public List<PhoneNumberType> phones {get;set;}

        public ContactType(){

            addresses = new List<ContactAddressType>();
            name = new NameType();
            emails = new List<EmailAddressType>();
            phones = new List<PhoneNumberType>();

        } 

    }
    
    public class ContactAddressType{

        public String street1 {get;set;}
        public String street2 {get;set;}
        public String aptNumber {get;set;}
        public String city {get;set;}
        public String subRegion {get;set;}
        public String region {get;set;}
        public String postalCode {get;set;}
        public String country {get;set;}
        public String addressType {get;set;}

    }
    
    public class EmailAddressType{

        public String emailAddress {get;set;}
        public String emailType {get;set;}

    }
    
    public class NameType{

        public String salutation {get;set;}
        public String firstName {get;set;}
        public String middleName {get;set;}
        public String lastName {get;set;}
        public String nameSuffix {get;set;}

    }
    
    public class PhoneNumberType{

        public String phoneNumber {get;set;}
        public String phoneType {get;set;}

    }
    
//=========Other Services   
    public class OtherServices_Output{

        public String resultCode {get;set;}
        public String resultMsg {get;set;}
        public String referenceId {get;set;}

    }

//===========Retransmit    
    public class Retransmit_Request{

        public String referenceId {get;set;}
        public Integer maximumLocationCount {get;set;}

    }

    public static void retransmit(String refId, Integer maxLocationCount, String programType){

        Sirius_XM_Service__c settings = Sirius_XM_Service__c.getOrgDefaults();
        String endpoint = settings.End_Point__c + '/' + programType.toLowerCase() + '/ReTransmit';

        Retransmit_Request sir_req = new Retransmit_Request();
        sir_req.referenceId = refId;
        sir_req.maximumLocationCount = maxLocationCount;
        
        String param = JSON.serialize(sir_req);
        System.debug('JSON: ' + param);
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(param); 

        Http http = new Http();
        
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);
            System.debug('THE RESPONSE: ' + res.getBody());
            
            System.debug('RESPONSE JSON 2 APEX ' + System.JSON.deserialize(res.getBody(), OtherServices_Output.class));
        }

    }

//===========AgentAssigned
    public class AgentAssigned_Request{

        public String referenceId{get;set;}
        public Boolean isAssigned {get;set;}

    }

    public static void agentAssigned(String refId, Boolean isAssign, String programType){

        Sirius_XM_Service__c settings = Sirius_XM_Service__c.getOrgDefaults();
        String endpoint = settings.End_Point__c + '/' + programType.toLowerCase() + '/AgentAssigned';

        AgentAssigned_Request sir_req = new AgentAssigned_Request();
        sir_req.referenceId = refId;
        sir_req.isAssigned  = isAssign;
        
        String param = JSON.serialize(sir_req);
        System.debug('JSON: ' + param);
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(param); 
        
        Http http = new Http();
        
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);
            System.debug('THE RESPONSE: ' + res.getBody());
        
            System.debug('RESPONSE JSON 2 APEX ' + System.JSON.deserialize(res.getBody(), OtherServices_Output.class));
        }

    }

//==========Terminate
    public class Terminate_Request{

        public String referenceId {get;set;}
        public String reasonCode {get;set;}

    }

    public static void terminate(String refId, String reasonCode, String programType){

        Sirius_XM_Service__c settings = Sirius_XM_Service__c.getOrgDefaults();
        String endpoint = settings.End_Point__c + '/' + programType.toLowerCase() + '/Terminate';

        Terminate_Request sir_req = new Terminate_Request();
        sir_req.referenceId = refId;
        sir_req.reasonCode  = reasonCode;
        
        String param = JSON.serialize(sir_req);
        System.debug('JSON: ' + param);
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(param); 
        
        Http http = new Http();
        if (!Test.isRunningTest()) {
            HTTPResponse res = http.send(req);
            System.debug('THE RESPONSE: ' + res.getBody());
        
            System.debug('RESPONSE JSON 2 APEX ' + System.JSON.deserialize(res.getBody(), OtherServices_Output.class));
        }

    }

//==========RequestService
    public class RequestService_Request{

        public String referenceId {get;set;}
        public String serviceType {get;set;}
        public String country {get;set;}

    }

    public static String requestService(String refId, String serviceType, String country, String programType){
        //return refId + serviceType + country + programType;
        try{

            Sirius_XM_Service__c settings = Sirius_XM_Service__c.getOrgDefaults();
            String endpoint = settings.End_Point__c + '/' + programType.toLowerCase() + '/RequestService';

            RequestService_Request sir_req = new RequestService_Request();
            sir_req.referenceId = refId;
            sir_req.serviceType  = serviceType;
            sir_req.country  = country;
            
            String param = JSON.serialize(sir_req);
            System.debug('JSON: ' + param);
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint);
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            req.setBody(param); 
            
            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug('THE RESPONSE: ' + res.getBody());
            
            OtherServices_Output reqServiceOut = (OtherServices_Output) JSON.deserialize(res.getBody(), OtherServices_Output.class);

            return reqServiceOut.resultCode;

        }
        catch(Exception ex){

            return ex.getMessage();
            
        }

    }
}