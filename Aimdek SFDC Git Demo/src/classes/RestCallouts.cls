public class RestCallouts{
    
    public static String getMercedesBenzBreakDownData(Id caseId) {
        try{
            system.debug('Case ID' +caseId);
            Case cc=[select CaseNumber, clientcasenumber__c, Current_Odometer__c, status, DI_Status_Date_Time__c, OL_Status_Date_Time__c, ER_Status_Date_Time__c, CL_Status_Date_Time__c,XX_Status_Date_Time__c,TW_Status_Date_Time__c, ETA__c,Trouble_code__c,Close_Reason_Non_Roadside__c,MBCProcessStep__c,Destination_Id__c,Destination_Name__c from case where id=:caseId];
            system.debug('without pretty'+JSON.serialize(cc));
            String JsonString=JSON.serialize(cc);
            return JsonString;
        }catch(System.Exception ex){System.debug(ex);}
        return null;
    }
    public static void sendNotDispatchEmailToMBC(Case c){
        try{
            Contact ctc = [select id, Email from Contact where email <> null limit 1];
            Automatic_email_recipients__c emails_addr = Automatic_email_recipients__c.getOrgDefaults();                     
            String[] toAddresses = new String[]{};
            if(emails_addr.MBC_Emails__c != null){
                for(String em : emails_addr.MBC_Emails__c.split(',')){
                    toAddresses.add(em);
                }
            }
            System.debug(toAddresses);
            if(emails_addr.MBC_Emails_Ext1__c != null){
                for(String em : emails_addr.MBC_Emails_Ext1__c.split(',')){
                    toAddresses.add(em);
                }
            }
            System.debug(toAddresses);
            if(emails_addr.MBC_Emails_Ext2__c != null){
                for(String em : emails_addr.MBC_Emails_Ext2__c.split(',')){
                    toAddresses.add(em);
                }
            }
            System.debug(toAddresses);
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@clubautoltd.com'];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
            mail.setToAddresses(toAddresses);
            mail.setTemplateId(CA__c.getOrgDefaults().MBC_Not_Dispatch_Email_Template__c);
            mail.setWhatId(c.id);
            mail.setSaveAsActivity(true); 
            mail.setTargetObjectId(ctc.id);       
            mail.setUseSignature(false);
            if(owea.size() > 0) 
            {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }  
            List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();        
            lstMsgs.add(mail);               
                    
            Savepoint sp = Database.setSavepoint();
            
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(lstMsgs);
            }
            
            Database.rollback(sp);
            //have to do trick with email template. 
            //it has to set targetobjectid as a contact id, then case inforation case be merged with template.
    
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
            for (Messaging.SingleEmailMessage email : lstMsgs) {
                Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                emailToSend.setToAddresses(email.getToAddresses());
                emailToSend.setHTMLBody(email.getHTMLBody());
                emailToSend.setSubject(email.getSubject());
                if(owea.size() > 0) 
                {
                    emailToSend.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                lstMsgsToSend.add(emailToSend);
            }
            
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(lstMsgsToSend);
            }
        }catch(Exception e){
             System.debug('Exception:'+e.getMessage());
        }
    }
    @future(callout=true)
    public static void callout(String url, String username, String password, String content, ID caseId,String reqMethod) {
        System.debug('URL:'+url);
        System.debug('content:'+content);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod(reqMethod); 
        req.setTimeout(5000);      
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        System.debug('BASIC Auth:'+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(content);
        try {
            HTTPResponse res;
            if(!test.IsRunningTest()){
                res = http.send(req);
            }
        } catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    public void mbcCallout(String url, String username, String password, String content, ID caseId,String reqMethod) {
        System.debug('URL:'+url);
        System.debug('content:'+content);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod(reqMethod); 
        req.setTimeout(5000);      
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        System.debug('BASIC Auth:'+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(content);
        try {
            HTTPResponse res;
            if(!test.IsRunningTest()){
                res = http.send(req);
            }
        } catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    @future(callout=true)
    public static void dispatchCase(ID caseId) {

           CA__c settings = CA__c.getOrgDefaults();
           System.debug('Dispatch Case ID ========= '+ settings.DI_Status__c);
           DispatchCallController dispatchCall = new DispatchCallController(caseId,settings.DI_Status__c,'20');
           dispatchCall.dispatchMBCCall();
   } 
   
    @future(callout=true)
    public static void updateServiceCall(ID caseId) {
           CA__c settings = CA__c.getOrgDefaults();
           System.debug('updateServiceCall ========= ');
           MBC_Integration mbc = new MBC_Integration(caseId,'23');
           mbc.mbcAuthorize();
    } 
}