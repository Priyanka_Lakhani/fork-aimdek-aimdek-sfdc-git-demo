public with sharing class CloseCase {
    
    public Case c { get; set; }
    public String mode { get; set; }
    public List<SelectOption> status_options { get; set; }
    
    public CloseCase(ApexPages.StandardController controller) {
        c = (Case)controller.getRecord();
        mode = 'Close';
        getCloseStatus();
        
        String status = ApexPages.currentPage().getParameters().get('s');
        if(status==NULL||status=='') { 
            status = CA__c.getOrgDefaults().CL_Status__c;
            c.Status=status; 
        }
        
        if(c.DI_Status_Date_Time__c!=NULL) { 
            mode = 'Cancel'; 
            c.Status=CA__c.getOrgDefaults().XX_Status__c;
        }
    }
    public void getCloseStatus() {
        status_options = new List<SelectOption>{
            new SelectOption(CA__c.getOrgDefaults().CL_Status__c, CA__c.getOrgDefaults().CL_Status__c),
            new SelectOption(CA__c.getOrgDefaults().XX_Status__c, CA__c.getOrgDefaults().XX_Status__c)
        };
    }
    public PageReference save() {
        if (mode=='Close'&& c.RecordType.DeveloperName=='Roadside_Assistance' && c.Kill_Code__c == null)
        {
            c.Kill_Code__c.addError('Please select a Close Reason');
            return null;
        }
            
        
        if (c.RecordType.DeveloperName=='Roadside_Assistance' && mode=='Cancel' && c.Close_Reason_Non_Roadside__c==null)
        {
            c.Close_Reason_Non_Roadside__c.addError('Please select a Cancel Reason');
            return null;
        }
         //TB SiriusXM start
        //if(mode == 'Close'){
                String msgerror = '';
                String caseSiriusId = c.Sirius_XM_ID__c;
                if(c.Sirius_XM_ID__c != null && c.Sirius_XM_ID__c != ''){
                
                    String siriusXMclienCode = '';
                    if(c.Program__r.Program_Code__c == '560'){
                    
                        siriusXMclienCode = 'honda';
                        
                    }
                    else if(c.Program__r.Program_Code__c == '563'){
                    
                        siriusXMclienCode = 'acura';
                        
                    }
                    try{
                    
                        SiriusXM_Service.terminate(caseSiriusId,'ReasonCode',siriusXMclienCode);
                        //c.Description = 'TEST SIRIUS FROM TAKECALL';
                        
                    }catch(exception ex){
                    
                        msgerror = 'Sirius XM Exceptiom :' + ex.getMessage();
                        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,msgerror);
                        //ApexPages.addMessage(myMsg);
                        
                    }
                }
                //TB SiriusXM end
        //}
        update c;
        if(mode=='Close') { return back(); }
        if(c.Status==CA__c.getOrgDefaults().XX_Status__c) { return new PageReference('/apex/DispatchCallUI?d=o&m=21&title=Cancel Service Call&Id='+c.Id).setRedirect(true); }
        return back();
    }
    public PageReference saveAndRoadside() {
        
        update c;
        
        if(c.Program__r.Program_Code__c =='566' || c.Program__r.Program_Code__c =='567' || c.Program__r.Program_Code__c =='568' || Test.isRunningTest()) {
            //OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'Melody Razavitoussi'];
            String msgBody2;
            //msgBody2 = '<p>Hello Ron,<br/>Service has been dispatched for an engineering vehicle.</p>';
            msgBody2 = '<p><b>Date/time: </b>' + c.LastModifiedDate + '<br/>';
            if(c.PickupStreet__c != NULL || c.PickupCity__c != NULL || c.PickupProvince__c != NULL || c.PickupCountry__c != NULL) {
                msgBody2 += '<b>Pick up location: </b> ' +  (c.PickupStreet__c != NULL ? c.PickupStreet__c : '') + (c.PickupCity__c != NULL ? ', ' + c.PickupCity__c : '') + (c.PickupProvince__c != NULL ? ', ' + c.PickupProvince__c : '') + (c.PickupCountry__c != NULL ? ', ' + c.PickupCountry__c : '') + '<br/>';
            }
            if(c.Destination_Name__c != NULL || c.Destination_Street__c != NULL || c.Destination_City__c != NULL || c.Destination_Province__c != NULL || c.Destination_Country__c != NULL) {
                msgBody2 += '<b>Drop off location: </b> ' +  (c.Destination_Name__c != NULL ? c.Destination_Name__c : '') + (c.Destination_Street__c != NULL ? ', ' + c.Destination_Street__c : '') + (c.Destination_City__c != NULL ? ', ' + c.Destination_City__c : '') + (c.Destination_Province__c != NULL ? ', ' + c.Destination_Province__c : '') + (c.Destination_Country__c != NULL ? ', ' + c.Destination_Country__c : '') + '<br/>';
            }
            msgBody2 += '<b>Case: </b>' + c.CaseNumber + '<br/>';
            if(c.Tow_Reason__c != '') {
                msgBody2 += '<b>Issue reported: </b>' + c.Tow_Reason__c + '</br>';
            }
            if(c.VIN_Member_ID__c != '') {
                msgBody2 += '<b>VIN: </b>' + c.VIN_Member_ID__c + '</br>';
            }
            if(c.First_Name__c != '') {
                msgBody2 += '<b>First Name: </b>' + c.First_Name__c + '</br>';
            }
            if(c.Last_Name__c != '') {
                msgBody2 += '<b>Last Name: </b>' + c.Last_Name__c + '</br>';
            }
            if(c.Phone__c != '') {
                msgBody2 += '<b>Phone Number: </b>' + c.Phone__c + '</br>';
            }
            if(c.Breakdown_Location__latitude__s != null && c.Breakdown_Location__longitude__s != null) {
                msgBody2 += '<b>Breakdown Location: </b>' + c.Breakdown_Location__latitude__s + ':' + c.Breakdown_Location__longitude__s + '</br>';
            }
            if(c.City__c != '') {
                msgBody2 += '<b>City: </b>' + c.City__c + '</br>';
            }
            if(c.Province__c != '') {
                msgBody2 += '<b>Province: </b>' + c.Province__c + '</br>';
            }
            if(c.Street__c != '') {
                msgBody2 += '<b>Street: </b>' + c.Street__c + '</br>';
            }
            if(c.Dealership__c != '') {
                msgBody2 += '<b>Dealership / Tow Destination: </b>' + c.Dealership__r.Name + '</p>';
            }
            
            //msgBody2 += '<p>Have a great day, <br/>Jaguar Land Rover roadside assistance.</p>';
            
            system.debug('msgBody2:' + msgBody2);
        
            Messaging.SingleEmailMessage mailRon = new Messaging.SingleEmailMessage();
            //String[] toAddresses = new String[] {'rsuther2@jaguarlandrover.com','mraz@clubautoltd.com'};
            String[] toAddresses = new String[] {'qa1@websolo.ca'};
            toAddresses.add('mraz@clubautoltd.com');
            mailRon.setToAddresses(toAddresses);
            mailRon.setSubject('Engineering vehicle roadside dispatch');
            mailRon.setPlainTextBody('');
            mailRon.setHtmlBody(msgBody2);
            /*if(owea.size() > 0) 
            {
                mailRon.setOrgWideEmailAddressId(owea.get(0).Id);
            } */
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailRon });
            }
        }
        
        return new PageReference('/apex/TakeCall?pid='+c.Id).setRedirect(true);
    }
    
    public PageReference back() {
        return new PageReference('/'+c.Id).setRedirect(true);
    }
}