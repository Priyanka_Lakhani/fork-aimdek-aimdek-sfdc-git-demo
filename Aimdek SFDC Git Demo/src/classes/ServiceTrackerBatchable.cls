global class ServiceTrackerBatchable implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Schedulable {

    private Id jobId;
    private String query;
    global ServiceTrackerBatchable(String query) {
        this.query = query;
        Service_Tracker__c s = Service_Tracker__c.getOrgDefaults();
        if (this.query == null) {
            this.query = 'SELECT Id, Club_Code__c, VIN_Member_ID__c FROM Case WHERE Club__r.Service_Tracker_Enabled__c=TRUE AND Program__r.Service_Tracker_Enabled__c=TRUE AND isClosed=FALSE AND Club_Call_Number__c<>null AND(Service_Tracker_Detail__c=null OR Service_Tracker_Detail__r.Minutes_Since_Update__c>=3)';
            if(s.Service_Tracker_Start_Date__c!=null) {
                DateTime d = DateTime.newInstance(s.Service_Tracker_Start_Date__c.year(), s.Service_Tracker_Start_Date__c.month(), s.Service_Tracker_Start_Date__c.day());
                
                this.query += ' AND CreatedDate >= ' + d.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            }
            this.query += ' ORDER BY CaseNumber ASC';
        }
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        try {
            jobId = bc.getJobID();
        } catch (Exception e) {
            jobId = '0';
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<SObject> batch) {
        List<Service_Tracker_Detail__c> service_calls = new List<Service_Tracker_Detail__c>();
        for(Case c : (List<Case>)batch) {
            if(!Test.isRunningTest()) { service_calls.addAll(ServiceTrackerIntegrationController.getServiceTrackerCalls(c)); }
        }
        if(!service_calls.isEmpty()) { Database.upsert(service_calls, Service_Tracker_Detail__c.UID__c); }
    }
    global void execute(SchedulableContext sc) {
        ServiceTrackerBatchable b = new ServiceTrackerBatchable(null); 
        database.executebatch(b,20);
    }
    global void finish(Database.BatchableContext bc) { 
        String new_job_name = 'Schedule_Service_Tracker';
        try {
            for (CronTrigger j : [SELECT Id, CronJobDetail.Name, State, NextFireTime FROM CronTrigger where CronJobDetail.Name=:new_job_name]) {
                System.Debug('Job: ' + j);
                System.abortJob(j.Id);
            }
        } catch (Exception e) {}
        Datetime now = System.now();
        now = now.addMinutes(1);
        String chron_exp = '' + now.second() + ' ' + now.minute() + ' ' + now.hour() + ' ' + now.day() + ' ' + now.month() + ' ? ' + now.year();
        System.schedule(new_job_name, chron_exp, new ServiceTrackerBatchable(null)); 
    }       
}