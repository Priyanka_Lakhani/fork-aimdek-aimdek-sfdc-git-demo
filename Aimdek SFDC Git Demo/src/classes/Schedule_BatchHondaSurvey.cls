global class Schedule_BatchHondaSurvey implements Schedulable {
    global void execute(SchedulableContext sc) {
      //string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case where status=\'Closed (Call Cleared)\' and Club_Call_Number__c!=null and email__c!=null and SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'JLR Survey\' and createddate= LAST_N_DAYS:15'; // limit 100';
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate from case where status=\'Closed (Call Cleared)\' and Club_Call_Number__c!=null and SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'Honda Survey\' and createddate= LAST_N_DAYS:15'; // limit 100';
        BatchSurvey b = new BatchSurvey(qry); 
        b.owa = 'Melody Razavitoussi';
        b.surveyname = 'Honda Survey';
        database.executebatch(b,20);
    }
}