public class SiriusXM_RequestServiceContrl{

	public String serviceType {get;set;}
    public String country {get;set;}
	public String msg {get;set;}
    public String programType;

    public List<String> getRequestServices() {
        Sirius_XM_Service__c sirius_XM = Sirius_XM_Service__c.getOrgDefaults();
        List<String> siriusRequestVal = new List<String>();

        if(sirius_XM.RequestServices__c != null){
            for(String val : sirius_XM.RequestServices__c.split(';')){
                siriusRequestVal.add(val);
            }
        }
        return siriusRequestVal;
    }

	public String ref_id;

    public SiriusXM_RequestServiceContrl(){
        
    	ref_id = ApexPages.currentPage().getParameters().get('ref_id');
        programType = ApexPages.currentPage().getParameters().get('pr_type');
        country = ApexPages.currentPage().getParameters().get('country');
        if(country.equalsIgnoreCase('ca') || country.equalsIgnoreCase('canada')){
            country = 'CANADA';
        }
        else if(country.equalsIgnoreCase('us') || country.equalsIgnoreCase('usa') || country.equalsIgnoreCase('united states')){
            country = 'USA';
        }
        else{
            country = 'UNKNOWN';
        }   
    	msg = '';
        serviceType = '';

    }

    public PageReference sendReq(){

    	if(ref_id != null && ref_id != '' && serviceType != '' && serviceType != null && country != '' && country != null && programType != null && programType != ''){

            msg = SiriusXM_Service.requestService(ref_id,serviceType,country,programType);

            if(msg == 'NO_ERROR'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,serviceType + ' Success'));
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
            }
    	}
    	else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid data.'));
    	}

        return null;

    }
    
}