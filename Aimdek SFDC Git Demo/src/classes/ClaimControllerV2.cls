public with sharing class ClaimControllerV2 {
    /* start */
    public String locale { get; set; }
    public boolean isFrench { get; set; }
    public boolean isUploadFile {get;set;}
    public String ClientValue {get;set;}
    /*end */    
    public Claim__c claim {set;get;}
    /* start */
    public Claim__c claim1 {set;get;}
    public Claim__c claim2 {set;get;}
    /*end */
    string claimFields {set;get;}
    public string progCode {set;get;}
    string progExtCode {set;get;}
    string progPromoCode {set;get;}    

    public boolean isSubmit {set;get;}
    public boolean isQuery {set;get;}
    public boolean isFAQ {set;get;}
    public boolean isDone {set;get;}
    /* start */
    public boolean isHome {set;get;}
    /*end */
    public boolean isRSAClaim {set;get;}
    public boolean isTrExClaim {set;get;}
    /* start*/
    public boolean isMeal {set;get;}
    public boolean isHotel {get;set;}
    public boolean isRental {get;set;}
    /* end */  
    public string queryresult {set;get;}
    
    public string qname {set;get;}
    public string qemail{set;get;}
    
    public boolean VINNotFound {set;get;}
    public boolean WrtyExpired {set;get;}
    //start   
    public string CheckClaimNoErrorResult {get;set;}
    public boolean CheckClaimNoErrorFound {get;set;}
    
    public string CheckClaimEmailErrorResult {get;set;}
    public boolean CheckClaimEmailErrorFound {get;set;}
    
   //end
    public integer claimCount {set;get;}
    public string claimNos {set;get;}
   //start
    public string queryStatus {set;get;}
    public string queryPaidAmount {set;get;}
    public string queryReason {set;get;}
    public string queryLastStatusUpdatedDate {set;get;}
    public string queryNote {set;get;}
    
    public Decimal breakDownToHomeDistance {get;set;}
    public string homeAddress{get;set;}
    public string breakdownLocation{get;set;}
    public string sessionId {get;set;}

    public boolean RSAttachmentNotFound {set;get;}
    public string RSAttachmentResult {get;set;}
    public boolean MealAttachmentNotFound {get;set;}
    public string MealAttachmentResult {get;set;}
    public boolean RentalAttachmentNotFound {get;set;}
    public string RentalAttachmentResult {get;set;}
    public boolean HotelAttachmentNotFound {get;set;}
    public string HotelAttachmentResult {get;set;}
    public boolean WorkorderAttachmentNotFound {get;set;}
    public string WorkorderAttachmentResult {get;set;}
    
    public integer FileCount {get; set;}
    public List<Attachment> rsFileList {get; set;}
    public List<Attachment> mealFileList {get; set;}
    public List<Attachment> hotelFileList {get; set;}
    public List<Attachment> rentalFileList {get; set;}
    public List<Attachment> workOrderFileList {get; set;}
    
   //end
    public ClaimControllerV2()
    {
        String locale = ApexPages.currentPage().getParameters().get('locale');
        if(locale == 'Fr'){
            locale = 'fr-ch';
            isFrench = true;
        }else{
            locale = 'en_US';
            isFrench = false;
        }
        isFAQ = true;
        isHome = true;
        progCode = '';
        progExtCode = '';
        progPromoCode = '';
        isUploadFile = false;
        claim = new Claim__c();
        claim1 = new Claim__c();     
        claim2 = new Claim__c();
        getClaimFields();
        System.debug('User Session id:'+UserInfo.getSessionId());
        sessionId = UserInfo.getSessionId();
        
        FileCount = 5 ;
        rsFileList = new List<Attachment>() ;
        mealFileList = new List<Attachment>() ;
        hotelFileList = new List<Attachment>() ;
        rentalFileList = new List<Attachment>() ;
        workOrderFileList = new List<Attachment>() ;
        
        for(Integer i = 1 ; i <= FileCount ; i++){
            rsFileList.add(new Attachment()) ;
            mealFileList.add(new Attachment()) ;
            hotelFileList.add(new Attachment()) ;
            rentalFileList.add(new Attachment()) ;
            workOrderFileList.add(new Attachment()) ;
        }
    }
    
    public void getClaimFields() {
        claimFields='';
        for(Schema.SObjectField f : Claim__c.SObjectType.getDescribe().fields.getMap().values()) {
            claimFields += (f.getDescribe().getName() + ',');
        }
        claimFields=claimFields.left(claimFields.length()-1);
    }
    public void SubmitNew()
    {
        this.isRSAClaim = false;
        this.isTrExClaim = false;
        this.isMeal = false;
        this.isHotel = false;
        this.isRental = false;
        isSubmit = true;
        isQuery = false;
        isFAQ=false; 
        isHome = false;   
        isDone = false;
        VINNotFound = false;
        //start
        WrtyExpired = false; 
        claimCount = 0;
        isUploadFile = false;
        claimNos = '';
        //end                    
        claim = new claim__c();
        claim1 = new Claim__c();  
        claim2 = new Claim__c();  
        System.debug('claim:'+claim);
        System.debug('claim1:'+claim1);
        System.debug('claim2:'+claim2); 
    }

    public void QueryRequest()
    {
        isSubmit = false;
        isQuery = true;
        isFAQ=false;
        isHome = false;
        isDone = false;
        queryresult = '';
        //start
        queryStatus = '';
        queryresult = '';
        queryPaidAmount ='';
        queryReason = '';
        queryLastStatusUpdatedDate = '';
        queryNote = '';
        qname = '';
        qemail = '';
        CheckClaimNoErrorFound = false;
        CheckClaimEmailErrorFound = false;
        CheckClaimNoErrorResult = '';
        CheckClaimEmailErrorResult ='';
        //end
    }

    //start
    public void Query()
    {
        try
        {           
            queryStatus = '';
            queryresult = '';
            queryPaidAmount ='';
            queryReason = '';
            queryLastStatusUpdatedDate = '';
            queryNote = '';
            
            CheckClaimNoErrorFound = false;
            CheckClaimEmailErrorFound = false;
            CheckClaimNoErrorResult = '';
            CheckClaimEmailErrorResult ='';
            
            
            if(validateCheckClaimCheckDetail()){
            
                claim = [select id,name,createddate,status__c,Last_Status_Changed_Date__c,Total_Paid_Amount__c,RejectionReason__c,Comment__c,email__c from claim__c where name=: qname limit 1];
                if(!((claim.name).equals(string.escapeSingleQuotes(qname)))){
                    CheckClaimNoErrorResult = Label.Claim_Number_Invalid_Message;
                    CheckClaimNoErrorFound = true;
                }
                if(!(claim.email__c).equals(string.escapeSingleQuotes(qemail))){
                    CheckClaimEmailErrorResult = Label.Claim_Number_And_Email_Not_Match_Message;
                    CheckClaimEmailErrorFound = true; 
                }
                if (claim != null && !CheckClaimNoErrorFound && !CheckClaimEmailErrorFound)
                {
                    if(claim.status__c == 'Received'){
                        queryresult = Label.Claim_Received_On_Label + ' '+ string.valueof(claim.createddate);
                        queryStatus = Label.Claim_Status_Label +': ' + claim.status__c;
                        queryNote = Label.Claim_Query_Information_Message;
                    }else if(claim.status__c == 'Paid'){
                        queryresult = Label.Claim_Received_On_Label + ' '+ string.valueof(claim.createddate);
                        queryStatus = Label.Claim_Status_Label +': ' + claim.status__c;
                        queryLastStatusUpdatedDate =Label.Claim_Last_Status_Changed_Date_Label +': ' + string.valueof(claim.Last_Status_Changed_Date__c);
                        queryPaidAmount = Label.Claim_Total_Amount_Paid_Label +': ' + claim.Total_Paid_Amount__c;
                        queryNote = Label.Claim_Query_Information_Message;
                    }else if(claim.status__c == 'Rejected'){
                        queryresult = Label.Claim_Received_On_Label + ' '+ string.valueof(claim.createddate);
                        queryStatus = Label.Claim_Status_Label +': ' + claim.status__c;
                        queryLastStatusUpdatedDate = Label.Claim_Last_Status_Changed_Date_Label +': ' +string.valueof(claim.Last_Status_Changed_Date__c);
                        queryReason = Label.Claim_Reason_Label +': ' + claim.RejectionReason__c;
                        queryNote = Label.Claim_Query_Information_Message;
                    }else if(claim.status__c == 'Partial payment'){
                        queryresult = Label.Claim_Received_On_Label + ' '+ string.valueof(claim.createddate);
                        queryStatus = Label.Claim_Status_Label +': ' + claim.status__c;
                        queryLastStatusUpdatedDate = Label.Claim_Last_Status_Changed_Date_Label +': ' +string.valueof(claim.Last_Status_Changed_Date__c);
                        queryPaidAmount = Label.Claim_Total_Amount_Paid_Label +': ' + claim.Total_Paid_Amount__c;
                        queryReason = Label.Claim_Reason_Label +': ' + claim.Comment__c;
                        queryNote = Label.Claim_Query_Information_Message;
                    }
                }
            }
            
        }
        catch(System.exception ex)
        {
                CheckClaimNoErrorResult = Label.Claim_Number_Invalid_Message;
                CheckClaimNoErrorFound = true;
                CheckClaimEmailErrorResult = Label.Claim_Number_And_Email_Not_Match_Message;
                CheckClaimEmailErrorFound = true; 
        }
    }
    //end
    
    public void FAQ()
    {
        isSubmit = false;
        isQuery = false;
        isFAQ=true;
        isHome = false;
        isDone = false;
    }
    
    public void Submit()
    { 
        RSAttachmentNotFound = false;
        RSAttachmentResult = '';
        if(validateClaimDetail()){  
            isUploadFile = true;   
            setProgramCode();
            claim.program__c = progCode;     

            if (this.isRSAClaim)
            {                    
                claim1 = claim.clone(true);
                claim1.ETECause__c=null;
                claim1.ETEDate__c=null;
                claim1.ETEHotel__c=null;
                claim1.ETELocation__c=null;
                claim1.ETEMeal__c=null;
                claim1.ETEOther__c=null;
                claim1.ETEOtherCause__c=null;
                claim1.ETERental__c=null;
                claim1.ETETrans__c=null;
                claim1.ClaimType__c = 'Roadside Service Claim';
                claim1.status__c = 'Received';
                
                Map<String, Schema.SObjectField> map1 = Schema.SObjectType.Claim__c.fields.getMap();
                for(String fieldName : map1.keySet()) 
                {
                    if(map1.get(fieldName).getDescribe().isUpdateable()) 
                    {
                        try
                        {
                            string st = string.valueof(claim1.get(fieldname)).toUpperCase();
                            claim1.put(fieldName , st);
                        }
                        catch(System.exception ex){}
                    }
                }
                if(VINNotFound || WrtyExpired)
                    claim1.AgentReview__c = true;
                else
                    claim1.AgentReview__c = false; 
                       
                List<Claim__c> listOfClaim = new List<Claim__c>();
                listOfClaim = [select id,Name,Vin__c,ServiceDate__c from Claim__c where Vin__c =: claim.vin__c and ServiceDate__c =: claim.ServiceDate__c]; 
        
                if(listOfClaim.size()>0){
                    claim1.Duplicate_Claim__c = true;
                }             
                insert claim1;    
                
                List<Attachment> listToInsert = new List<Attachment>() ;
                for(Attachment a: rsFileList)
                {
                    if(a.name != '' && a.name != '' && a.body != null)
                        listToInsert.add(new Attachment(parentId = claim1.Id, name = a.name, body = a.body)) ;
                }
                insert listToInsert ;
                
                claim1 = [Select id,Name from Claim__c where id =: claim1.id];
            }            
    
            if (this.isTrExClaim)
            {
                claim2 = claim.clone(true);
                claim2.ProviderCity__c=null;
                claim2.ProviderName__c=null;
                claim2.ProviderPhone__c=null;
                claim2.ProviderPostalcode__c=null;
                claim2.ProviderProvince__c=null;
                claim2.ProviderStreet__c=null;
                claim2.Service__c=null;
                claim2.ServiceDate__c=null;
                claim2.Amount__c=null;
                claim2.ClaimType__c = 'Emergency Travel Expense Claim';
                claim2.status__c = 'Received';
                claim2.DistanceEligibility__c = calculateDistanceEligibility();
                
                Map<String, Schema.SObjectField> map1 = Schema.SObjectType.Claim__c.fields.getMap();
                for(String fieldName : map1.keySet()) 
                {
                    if(map1.get(fieldName).getDescribe().isUpdateable()) 
                    {
                        try
                        {
                            string st = string.valueof(claim2.get(fieldname)).toUpperCase();
                            claim2.put(fieldName , st);
                        }
                        catch(System.exception ex){}
                    }
                }
                if(VINNotFound || WrtyExpired)
                    claim2.AgentReview__c = true;
                else
                    claim2.AgentReview__c = false;  
                    
                List<Claim__c> listOfClaim = new List<Claim__c>();
                listOfClaim = [select id,Name,Vin__c,ETEDate__c from Claim__c where Vin__c =: claim.vin__c and ETEDate__c =:claim.ETEDate__c]; 
        
                if(listOfClaim.size()>0){
                    claim2.Duplicate_Claim__c = true;
                }   
                insert claim2;
                claim2 = [Select id,Name from Claim__c where id =: claim2.id];
            }
        } 
    }
    
    public void Clear()
    {
        claim = new claim__c();
    }
    
    public void vinChanged()
    {
        if(claim.vin__c == null){
            return;
        }

        VINNotFound = true;
        WrtyExpired = true;
        VIN__c vin = null;
        setProgramCode();

        try{
            vin = [select id,Base_Warranty_Start__c,Base_Warranty_End__c,Base_Warranty_Max_Odometer__c from vin__c where name =: claim.vin__c and program__r.Program_Code__c =: progCode and inactive__c != true limit 1];      
        }catch(System.Exception ex)
        {}
        if (vin != null)
        {
            VINNotFound = false; 
            if (date.valueof(vin.Base_Warranty_End__c) > system.today())
                WrtyExpired = false;
            else
                return;
            
        }
                
        if ((VINNotFound && progExtCode != '') || (!VINNotFound && WrtyExpired))
        {
            try{
                vin = [select id,Extended_Warranty_Start__c,Extended_Warranty_End__c,Extended_Warranty_Max_Odometer__c from vin__c where name =: claim.vin__c and Extended_Program__r.Program_Code__c =: progExtCode and inactive__c != true limit 1];
            }catch(System.Exception ex)
            {}
            
            if (vin != null)
            {
                VINNotFound = false; 
                
                if (date.valueof(vin.Extended_Warranty_End__c) > system.today())
                    WrtyExpired = false;
                else
                    return;
            }
        }
        
        
        if ((VINNotFound && progExtCode != '') || (!VINNotFound && WrtyExpired))
        {
            try{
                vin = [select id,Promo_Start__c,Promo_End__c,Promo_Max_Odometer__c from vin__c where name =: claim.vin__c and Promo_Program__r.Program_Code__c =: progPromoCode and inactive__c != true limit 1];
            }
            catch(System.Exception ex)
            {}
        
            if (vin != null)
            {
                VINNotFound = false; 
                
                if (date.valueof(vin.Promo_End__c) > system.today())
                    WrtyExpired = false;
                else
                    return;
            }
        }
        
    }
    
    //start    
    public boolean validateRSAClaimAttachment(){
        for(Attachment a: rsFileList)
        {
            if(a.name != '' && a.name != '' && a.body != null)
                return false;
        }
        RSAttachmentNotFound = true;
        RSAttachmentResult = 'Please upload Copy of Tow or Roadside Service Receipts';
        return true;
    }
    public boolean validateClaimDetail(){
        boolean validateMessage = true;     
        vinChanged();
        if(String.ISBLANK(claim.VIN__c)){
             claim.VIN__c.addError(Label.Claim_VIN_Not_Blank_Error_Message);
             validateMessage = false;
        }if(String.ISBLANK(claim.Reenter_VIN__c)){
            claim.Reenter_VIN__c.addError(Label.Claim_Reenter_VIN_Not_Blank_Error_Message);
            validateMessage = false;
        }if(String.isNotBlank(claim.Reenter_VIN__c) && claim.VIN__c != claim.Reenter_VIN__c){
            claim.Reenter_VIN__c.addError(Label.Claim_Reenter_VIN_Not_Match_Error_Message);
            validateMessage = false;
        }if(String.ISBLANK(claim.FirstName__c)){
              claim.FirstName__c.addError(Label.Claim_First_Name_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.LastName__c)){
              claim.LastName__c.addError(Label.Claim_Last_Name_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.Email__c)){
              claim.Email__c.addError(Label.Claim_Email_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.Email1__c)){
              claim.Email1__c.addError(Label.Claim_Reenter_Email_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.isNotBlank(claim.Email__c) && String.isNotBlank(claim.Email1__c) && claim.Email__c != claim.Email1__c){
               claim.Email1__c.addError(Label.Claim_Email_Not_Match_Error_Message);
               validateMessage = false;
        }if(String.ISBLANK(claim.Phone__c)){
              claim.Phone__c.addError(Label.Claim_Residence_Phone_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.Street__c)){
              claim.Street__c.addError(Label.Claim_Address_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.City__c)){
              claim.City__c.addError(Label.Claim_City_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.PostalCode__c)){
              claim.PostalCode__c.addError(Label.Claim_Postal_Code_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.Country__c)){
              claim.Country__c.addError(Label.Claim_Country_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.Province__c)){
              claim.Province__c.addError(Label.Claim_Province_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && String.ISBLANK(claim.Service__c)){
              claim.Service__c.addError(Label.Claim_Service_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && claim.ServiceDate__c == null){
              claim.ServiceDate__c.addError(Label.Claim_Service_Date_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && claim.ServiceDate__c != null && claim.ServiceDate__c > System.Today()){
              claim.ServiceDate__c.addError(Label.Claim_Service_Date_Not_In_Future_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && String.ISBLANK(claim.ProviderName__c)){
              claim.ProviderName__c.addError(Label.Claim_RS_Establishment_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && String.ISBLANK(claim.ProviderStreet__c)){
              claim.ProviderStreet__c.addError(Label.Claim_Address_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && String.ISBLANK(claim.ProviderCity__c)){
              claim.ProviderCity__c.addError(Label.Claim_City_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && String.ISBLANK(claim.ProviderProvince__c)){
              claim.ProviderProvince__c.addError(Label.Claim_Province_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && String.ISBLANK(claim.ProviderPostalcode__c)){
              claim.ProviderPostalcode__c.addError(Label.Claim_Postal_Code_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && claim.Amount__c == null){
              claim.Amount__c.addError(Label.Claim_Total_Amount_Paid_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isRSAClaim && validateRSAClaimAttachment()){
            validateMessage = false;
        }if(this.isTrExClaim && String.ISBLANK(claim.ETELocation__c)){
              claim.ETELocation__c.addError(Label.Claim_Location_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && claim.ETEDate__c == null){
              claim.ETEDate__c.addError(Label.Claim_Date_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && claim.ETEDate__c != null && claim.ETEDate__c > System.today()){
              claim.ETEDate__c.addError(Label.Claim_Date_Not_In_Future_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && String.ISBLANK(claim.ETECause__c)){
              claim.ETECause__c.addError(Label.Claim_Cause_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && String.isNotBlank(claim.ETECause__c) && claim.ETECause__c == 'Other' && String.ISBLANK(claim.ETEOtherCause__c)){
              claim.ETEOtherCause__c.addError(Label.Claim_Other_Cause_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && this.isMeal && claim.ETEMeal__c == null){
              claim.ETEMeal__c.addError(Label.Claim_ETEMeal_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && this.isHotel && claim.ETEHotel__c == null){
              claim.ETEHotel__c.addError(Label.Claim_ETEHotel_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && this.isRental && claim.ETERental__c == null){
              claim.ETERental__c.addError(Label.Claim_ETERental_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && claim.ETETrans__c == null){
              claim.ETETrans__c.addError(Label.Claim_Transportation_Not_Blank_Error_Message);
              validateMessage = false;
        }if(this.isTrExClaim && claim.ETEOther__c == null){
              claim.ETEOther__c.addError(Label.Claim_ETEOther_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.ClaimReason__c)){
              claim.ClaimReason__c.addError(Label.Claim_Notes_Not_Blank_Error_Message);
              validateMessage = false;
        }if(String.ISBLANK(claim.OwnerName__c)){
              claim.OwnerName__c.addError(Label.Claim_Claimant_Name_Not_Blank_Error_Message);
              validateMessage = false;
        }if(!claim.isCertified__c){
              claim.isCertified__c.addError(Label.Claim_Select_Certify_Error_Message);
              validateMessage = false;
        }
        return validateMessage;
    }
     public boolean validateCheckClaimCheckDetail(){
        boolean validateMessage = true;
        if(String.ISBLANK(qname)){
            CheckClaimNoErrorResult = Label.Claim_Number_Not_Blank_Error_Message;
            CheckClaimNoErrorFound = true;
            validateMessage = false;
        }
        if(String.ISBLANK(qemail)){
            CheckClaimEmailErrorResult = Label.Claim_Email_Not_Blank_Error_Message;
            CheckClaimEmailErrorFound = true;            
            validateMessage = false;
        }else{
            queryresult = '';
        }
        return validateMessage;
    }      
    //end 
    
    public Decimal calculateDistanceEligibility(){
        decimal distanceCoverage = 0;
        try{
            homeAddress = EncodingUtil.urlEncode(
                + claim.Street__c + ' '
                + claim.City__c + ', '
                + claim.Province__c + ' '
                + claim.PostalCode__c + ' '
                + claim.Country__c,
                'UTF-8');
             system.debug(homeAddress);
            breakdownLocation = EncodingUtil.urlEncode(
                claim.ETELocation__c,
                'UTF-8');
                system.debug(breakdownLocation);
            googleMaps gm = new googleMaps(homeAddress,breakdownLocation);
            distanceCoverage = gm.distance;
        }catch(System.Exception ex){
            System.debug('Distance calculation Exception:'+ex);
        }
        return distanceCoverage;
    }        
    
     public PageReference ChangeToEngLanguage() {
        String localeStr = null;
        isFrench = !isFrench;
        localeStr = 'en';
        PageReference pageRef = new PageReference('/apex/'+ApexPages.currentPage().getUrl().substringBetween('apex/', '?'));
        pageRef.setRedirect(true);
        pageRef.getParameters().put('locale',localeStr);
        return pageRef;
    }
     public PageReference ChangeToFrLanguage() {
        String localeStr = null;
        isFrench = !isFrench;
        localeStr = 'Fr';
        PageReference pageRef = new PageReference('/apex/'+ApexPages.currentPage().getUrl().substringBetween('apex/', '?'));
        pageRef.setRedirect(true);
        pageRef.getParameters().put('locale',localeStr);
        return pageRef;
    }
    public void setSubmitResult(){
        isSubmit = false;
        isQuery = false;
        isFAQ=false;
        isHome = false;
        isDone = true;
        System.debug('claim1:'+claim1);
        System.debug('claim2:'+claim2);
        if(claim1 != null  && claim1.id != null && String.isNotBlank(claim1.Name)){
            claimNos = claim1.Name;
            claimCount +=1;
        }
        if(claim2 != null  && claim2.id != null && String.isNotBlank(claim2.Name)){
            claimCount+=1;
            if (claimNos != '')
                claimNos += ',' + claim2.Name;
            else
                claimNos = claim2.Name;
        }
    }
    public void setProgramCode(){
        if(ClientValue == 'kia')
            progCode = progExtCode = progPromoCode = '503';
        else if(ClientValue == 'hyundai')
            progCode = progExtCode = progPromoCode = '505';
        else if(ClientValue == 'honda')
            progCode = progExtCode = '560';
        else if(ClientValue == 'jlr')
            progCode = progExtCode = progPromoCode = '864';
        else if(ClientValue == 'subaru')
            progCode= progExtCode = progPromoCode = '506';
        else if(ClientValue == 'acura')
            progCode= progExtCode = progPromoCode = '563';
    }
}