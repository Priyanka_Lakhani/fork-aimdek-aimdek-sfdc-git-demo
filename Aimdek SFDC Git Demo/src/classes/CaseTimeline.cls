public with sharing class CaseTimeline {
    public class CaseTimelineRecord {
        public String label { get; set; }
        public Event e { get; set; }
        public Boolean elapsed { get; set; }
        public Boolean serviceTracker { get; set; }
        public CaseTimelineRecord(){}
        public CaseTimelineRecord(String label, Datetime statusDateTime) {
            this.label = label;
            this.e = new Event(EndDateTime=statusDateTime);
            this.serviceTracker=false;
            this.elapsed=false;
        }
        public CaseTimelineRecord(String label, Datetime statusDateTime, Boolean showServiceTracker) {
            this.label = label;
            this.e = new Event(EndDateTime=statusDateTime);
            this.serviceTracker=showServiceTracker;
            this.elapsed=false;
        }
    }
    
    public List<CaseTimelineRecord> cRecords { get; private set; }
    public List<CaseTimelineRecord> iRecords { get; private set; }
    public List<CaseTimelineRecord> records { get; private set; }
    public Integer index { get; private set; }
    Integer offset;
    public Case c { get; set; }
    Id cid;
    public Boolean showOpenMapBtn {get;set;}
    

    public CaseTimeline(ApexPages.StandardController controller) {
        cid = ApexPages.currentPage().getParameters().get('Id');
        
        init();

    }
    

    public void init() {
        
        c = [Select Id, Final_ETA__c, Final_ETA_Date_Time__c, Service_Tracker_Detail__c, Service_Tracker_ETA__c, Service_Tracker_Detail__r.ETA_Date_Time__c, CaseNumber, Status, Club__r.Time_Offset__c, RE_Status_Date_Time__c, SP_Status_Date_Time__c, DI_Status_Date_Time__c, ER_Status_Date_Time__c, OL_Status_Date_Time__c, TW_Status_Date_Time__c, CL_Status_Date_Time__c,Service_Tracker_UID__c,Service_Tracker_UID_Program_Code__c,Service_Tracker_Detail__r.UID__c,XX_Status_Date_Time__c FROM Case where Id=: cid LIMIT 1];
        offset=(Integer.valueOf(c.Club__r.Time_Offset__c!=NULL?c.Club__r.Time_Offset__c:0));
        records = new List<CaseTimelineRecord> {
            new CaseTimelineRecord('Call Received', c.RE_Status_Date_Time__c),
            new CaseTimelineRecord('Call Spotted', c.SP_Status_Date_Time__c),
            new CaseTimelineRecord('Dispatched', c.DI_Status_Date_Time__c),
            new CaseTimelineRecord('Driver En Route', c.ER_Status_Date_Time__c),
            new CaseTimelineRecord('Driver On Location', c.OL_Status_Date_Time__c, true),
            new CaseTimelineRecord('Vehicle in Tow', c.TW_Status_Date_Time__c)
        };
        CaseTimelineRecord ctr = new CaseTimelineRecord();     
        if(c.XX_Status_Date_Time__c  != null)
            ctr = new CaseTimelineRecord('Call Cancelled', c.XX_Status_Date_Time__c);
        else
            ctr = new CaseTimelineRecord('Call Cleared', c.CL_Status_Date_Time__c);
        records.add(ctr);
        for(CaseTimelineRecord t : records) {
            if(t.e.EndDateTime!=NULL) { t.e.EndDateTime=t.e.EndDateTime.addHours(offset); }
        }
            if (c.Status=='Closed (Call Cleared)' || c.status == 'Call Cancelled') {
            index = 7;
        } else if (c.Status=='Vehicle in Tow') {
            index = 6;
        } else if (c.Status=='Driver On Location') {
            index = 5;
        } else if (c.Status=='Driver En Route') {
            index = 4;
        } else if (c.Status=='Dispatched') {
            index = 3;
        } else if (c.Status=='Spotted') {
            index = 2;
        } else if (c.Status=='Received') {
            index = 1;
        } else {
            index = 0;
        }
        for(Integer i = 0; i <index; i++) { records[i].elapsed=true; }
        

         if ((c.Status=='Vehicle in Tow') ||
            (c.Status=='Driver On Location') ||
            (c.Status=='Driver En Route') ||
            (c.Status=='Dispatched') ||
            (c.Status=='Spotted'))
                    showOpenMapBtn = true;
            else
                    showOpenMapBtn = false;
                    
        if (showOpenMapBtn)
        {
            try
            {
                Service_Tracker_Detail__c st = [select id from Service_Tracker_Detail__c where UID__c = :c.Service_Tracker_UID__c OR UID__c = :c.Service_Tracker_UID_Program_Code__c  limit 1];
            }
            catch(System.Exception ex)
            {
                showOpenMapBtn = false;
            }
        }
        
        showOpenMapBtn = true;
        
    }           

}