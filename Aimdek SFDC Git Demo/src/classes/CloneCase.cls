public class CloneCase {

    Case thisCase;
    string cloneReason {get;set;}
    public Case clone { get; set; }
    public CloneCase(ApexPages.StandardController controller) {
        thisCase = (Case)controller.getRecord();
        cloneReason = ApexPages.currentPage().getParameters().get('CR');
        system.debug('cr:' + cloneReason);
    }

    public PageReference init() {
        CaseExtension c = new CaseExtension();
        c.c=new Case(Id=thisCase.Id);
        c.refresh();
        clone=c.c.clone(false);
        
        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.Clear_Fields_on_Clone.getFields()) {
            try { clone.put(f.getFieldPath(), NULL); } catch(Exception e) {}
        }
        clone.Status=CA__c.getOrgDefaults().RE_Status__c;
        clone.OwnerId=userInfo.getUserId();
        if (String.isNotEmpty(c.c.Club_Call_Number__c) && String.isNotEmpty(c.c.Club__c))
        	clone.IsRedispatch__c = true;
        else
        	clone.IsRedispatch__c = false;
        clone.ParentId = thisCase.Id;
        clone.cloneReason__c = cloneReason;
        insert clone;
        return new PageReference('/apex/TakeCall?Id='+clone.Id).setRedirect(TRUE);
    }    
}