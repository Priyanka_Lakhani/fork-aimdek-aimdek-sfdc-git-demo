global class BatchSurvey implements Database.Batchable<SObject>, Database.Stateful {

    public string surveyname {get;set;}
    private Id jobId;
    private String query;
    public string owa {get;set;}
    global BatchSurvey(String qry) {
        //this.surveyname = qry;
        this.query = qry; //'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case where email__c!=null and SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'' + surveyname +'\' '; // limit 100';
        system.debug('qeury:' + query);
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        try {
            jobId = bc.getJobID();
        } catch (Exception e) {
            jobId = '0';
        }
        return Database.getQueryLocator(query);
    }
    

    global void execute(Database.BatchableContext bc, List<SObject> batch) {
        
        system.debug('1');
        
        List<Case> cases = (list<Case>)batch;
        system.debug('2');
        
        id surveyemailid;

        try
        {
            //surveyemailid = [select id from OrgWideEmailAddress where DisplayName = 'JLR Survey' limit 1].id;
            surveyemailid = [select id from OrgWideEmailAddress where DisplayName = :owa limit 1].id;
            
                    system.debug('3');
            
            system.debug('surveyemailid:' + surveyemailid);
        }
        catch(System.Exception ex)
        {
            system.debug('no survey '  + surveyname +' found');
            return;
        }
        
        string urlprefix = Sites__c.getAll().get('Survey').Value__c;
        
        set<id> surveyids = new set<id>();
        //list<SurveyResponse__c> resps = new list<SurveyResponse__c>();
        Messaging.SingleEmailMessage[] mails =   new List<Messaging.SingleEmailMessage>();
        
        //list<case> cases= [select id,casenumber,first_name__c,last_name__c, email__c,Vehicle_Make__c,Vehicle_Model__c,Trouble_Code__c,program__r.survey__c,surveysent__c  from case where email__c!=null and SurveySent__c = null and program__r.survey__c !=null limit 100];
        set<string> unsubemails = new set<string>();
        set<string> emails = new set<string>();
        
        for (case c : cases)
        {
            surveyids.add(c.program__r.survey__c);
            if(c.email__c != NULL) {
                if (!emails.contains(c.email__c)) {
                   emails.add(c.email__c);
                }
            }
        }
        
        list<SurveyUnsubscriber__c> SurveyUnsubscriberemails = [select email__c from SurveyUnsubscriber__c where email__c in :emails];
        
        for (SurveyUnsubscriber__c sub : SurveyUnsubscriberemails)
        {
            if (!unsubemails.contains(sub.email__c))
                unsubemails.add(sub.email__c);
        }
        
        system.debug('unsubemails:' + unsubemails);
        
        list<Survey_Question__c> questions = [select id, Survey__c,question__c,type__c,order__c,choices__c from Survey_Question__c where Survey__c in :surveyids order by survey__c,order__c];
        boolean toupdcase = false;
        
        for (case c: cases)
        {
            system.debug('c.email:' + c.email__c);
            
            if (c.email__c ==null || unsubemails.contains(c.email__c))
                continue;
            
            boolean hasSurvey = false;
            for (Survey_Question__c sq: questions)
            {
                if (sq.survey__c == c.program__r.survey__c)
                {
                    //SurveyResponse__c resp = new SurveyResponse__c();    
                    //resp.case__c = c.id;
                    //resp.SurveyQuestion__c = sq.id;
                    //resps.add(resp);
                    toupdcase = true;
                    hasSurvey= true;
                }
            }
            
            if (hasSurvey)
            {
                //string dt = string.valueof(c.CreatedDate.year()) + string.valueof(c.CreatedDate.month()) + string.valueof(c.CreatedDate.day());
                c.surveysent__c = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c).replace('+','%2B').replace('/','%2F');
                //c.surveysent__c = GlobalClass.ScrambleText(c.casenumber);
                system.debug('email:' + c.email__c);
                system.debug('url:' + c.surveysent__c);
                string country = '';
                string brand = '';

                if (c.program__r.program_code__c == '858' || c.program__r.program_code__c == '859' || c.program__r.program_code__c == '860' || c.program__r.program_code__c == '861' || c.program__r.program_code__c == '862' || c.program__r.program_code__c == '863' || c.program__r.program_code__c == '864' || c.program__r.program_code__c == '864' || c.program__r.program_code__c == '865'||c.program__r.program_code__c == '866'||c.program__r.program_code__c == '867') {
                    country = 'US';
                    if (c.program__r.program_code__c == '864' || c.program__r.program_code__c == '865'||c.program__r.program_code__c == '866'||c.program__r.program_code__c == '867')
                        country = 'CA';
                    brand = 'Jaguar';
                    
                    if (c.vehicle_make__c != 'Jaguar') {
                        brand = 'Land Rover';
                    }
                        
                    mails.add(buildJLREmail(new string[] {c.email__c},c.SurveySent__c,surveyemailid,urlprefix,c.first_name__c,country,brand,c.program__r.program_code__c));
                }

                if (c.program__r.program_code__c == '560' || c.program__r.program_code__c == '561' || c.program__r.program_code__c == '562' || c.program__r.program_code__c == '563' || c.program__r.program_code__c == '564' || c.program__r.program_code__c == '565' || c.program__r.program_code__c == '566' || c.program__r.program_code__c == '567' || c.program__r.program_code__c == '568' || c.program__r.program_code__c == '569') {
                    country = 'CA';
                    if (c.program__r.program_code__c == '560' || c.program__r.program_code__c == '561' || c.program__r.program_code__c == '562') {
                        brand = 'Honda Canada Inc.';
                    }
                    else if (c.program__r.program_code__c == '563' || c.program__r.program_code__c == '564' || c.program__r.program_code__c == '565') {
                        brand = 'Acura Canada Inc.';
                    }
                    else if (c.program__r.program_code__c == '566' || c.program__r.program_code__c == '567' || c.program__r.program_code__c == '568') {
                        brand = 'Acura NSX Canada Inc.';
                    }
                    else if (c.program__r.program_code__c == '569') {
                        brand = 'Honda Motorcycle';
                    }
                    mails.add(buildHondaMail(new string[] {c.email__c},c.SurveySent__c,surveyemailid,urlprefix,c.first_name__c,country,brand,c.program__r.program_code__c));
                }

                //c.surveysent__c = '';
            }
        }
        //system.debug(resps);
        if (toupdcase)
        {
            //insert resps;
            update cases;
            if (!Test.isRunningTest()) {
                Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
                system.debug('results:' + results);
            }    
        }
    }
    global void finish(Database.BatchableContext bc) { }
    
    global Messaging.SingleEmailMessage buildJLREmail(string[] emails,string url,id surveyemailid,string urlprefix,string name,string country,string brand,string prgid)
    {
        list<string> bccemails = new list<string>();
        
        
        string email = emails[0].trim() +',' +prgid;
        email = GlobalClass.ScrambleText(email).replace('+','%2B').replace('/','%2F');
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        try
        {
            bccemails.add(Sites__c.getAll().get('JLRSurveyBcc').Value__c);
            mail.bccAddresses = bccemails;
        }
        catch(System.exception ex){}
        
        mail.toAddresses = emails;
        
        //mail.setReplyTo('noreply@JLR.com');
        //    mail.SetSenderDisplayName ('noreply@JLR.salesforce.com');
        //mail.setTemplateID(Templates__c.getAll().get('JLRSurvey').tempId__c);

        mail.setOrgWideEmailAddressId(surveyemailid);
        mail.setSubject(brand + ' Roadside Assistance Survey');
        mail.setUseSignature(false);
                
        string msgbody ='';
        
        msgbody += '<font face="Arial" size="5"><b>We Value Your Feedback</b></font><br/><br/>';
        msgbody += '<font face="Arial" size="3.5"><p>';
        if(name != null && name != '') {
            msgbody += 'Dear ' + name + ',<br/>';
        }
        else {
            msgbody += 'Dear client' + ',<br/>';
        }    
        msgbody += '</p>';
        
        msgbody += '<p>';
        msgbody += 'You are receiving this survey because you recently utilized the ' + brand +' Roadside Assistance service.<br/>'; 
        msgbody += 'We would appreciate feedback on your recent experience as it is valuable to improve the experience for all customers.</p>';
        
        msgbody += '<p>';
        msgbody += 'Please click on the link below to complete this simple 6 question survey.<br/>';
        msgbody += ''+' <a href="' + urlprefix +'?id=' + url +'">click here</a></p>';

        msgbody += '<p>';
        msgbody += 'We appreciate your time.</p>';
        
        msgbody += '<p>';
        msgbody += brand + ' Roadside Assistance<br/>';
        msgbody +='<font face="Arial" size="2">';
        if (country== 'US')
        {
            msgbody += 'Jaguar Land Rover North America, LLC 555 MacArthur Blvd, Mahwah, NJ 07431<br/>';
        }
        else
        {
            msgbody += 'Jaguar Land Rover Canada ULC 75 Courtneypark Drive W, Unit # 3, Mississauga, L5W 0E3<br/>';
        }
        msgbody +='</font>';
        msgbody += '</p>';

        msgbody += '<font face="Arial" size="2"><p>';
        if (country== 'US')
        {
            msgbody += 'This survey is managed on behalf of Jaguar Land Rover North America, LLC by ClubAuto Roadside Services, Ltd.';
        }
        else
        {
            msgbody += 'This survey is managed on behalf of Jaguar Land Rover Canada ULC by ClubAuto Roadside Services, Ltd.';
        }
        msgbody += '</p>';
        msgbody += '<p>If you do not wish to receive any further communication regarding this survey, please click <a href="' + urlprefix +'?unsubscribe='+email +'">here</a></p>';
        msgbody += '</font>';
        mail.setHtmlBody(msgbody);
        
        //mail.setwhatId(caseid);
        
        system.debug('email:' + mail);
        return mail;
    }

    global Messaging.SingleEmailMessage buildHondaMail(string[] emails,string url,id surveyemailid,string urlprefix,string name,string country,string brand,string prgid)
    {
        string email = emails[0].trim() +',' +prgid;
        email = GlobalClass.ScrambleText(email).replace('+','%2B').replace('/','%2F');
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.toAddresses = emails;
        mail.setOrgWideEmailAddressId(surveyemailid);
        mail.setSubject(brand + ' Roadside Assistance Survey');
        mail.setUseSignature(false);
                
        string msgbody ='';
        msgbody += '<font face="Arial" size="5"><b>We Value Your Feedback</b></font><br/><br/>';
        msgbody += '<font face="Arial" size="3.5"><p>';
        if(name != null && name != '') {
            msgbody += 'Dear ' + name + ',<br/>';
        }
        else {
            msgbody += 'Dear client' + ',<br/>';
        }    
        msgbody += '</p>';
        msgbody += '<p>';
        msgbody += 'You are receiving this survey because you recently utilized the ' + brand +' Roadside Assistance service.<br/>'; 
        msgbody += 'To ensure your service request was dealt with in an efficient and professional manner, we would appreciate your feedback.</p>';
        msgbody += '<p>';
        msgbody += 'Please click on the link below to complete this simple 7 question survey.<br/>';
        msgbody += ''+' <a href="' + urlprefix +'?id=' + url +'">click here</a></p>';
        msgbody += '<p>';
        msgbody += 'We appreciate your time.</p>';
        msgbody += '<p>';
        msgbody += brand + ' Roadside Assistance<br/>';
        msgbody += '</p>';
        mail.setHtmlBody(msgbody);
        
        system.debug('email:' + mail);
        return mail;
    }
}