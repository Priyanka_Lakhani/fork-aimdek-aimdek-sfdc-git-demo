global class ReprocessIntegration implements Database.Batchable<SObject>, Database.Stateful {

    private Id jobId;
    private String query;
    
    global ReprocessIntegration(String query, Date starting, Date ending, Id UserId) {
        this.query = query;
        if (this.query == null) {
            this.query = 'SELECT Id, Parameters__c FROM Integration__c WHERE Id<>NULL ';
        }
        
        if(starting!=null) {
            DateTime s = DateTime.newInstance(starting.year(), starting.month(), starting.day());
            this.query += ' AND CreatedDate >= ' + s.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }
        if(ending!=null) {
            DateTime e = DateTime.newInstance(ending.year(), ending.month(), ending.day());
            this.query += ' AND CreatedDate <= ' + e.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }
        if(UserId!=null) {
            this.query += ' AND CreatedById=\''+UserId+'\'';
        }
        this.query += ' ORDER BY Case__c ASC, CreatedDate ASC';
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        try {
            jobId = bc.getJobID();
        } catch (Exception e) {
            jobId = '0';
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<SObject> batch) {
        
        CA_Integration ca = new CA_Integration();
        List<Integration__c> intUpdates = new List<Integration__c>();
        for(Integration__c i : (List<Integration__c>)batch) {
            ca.direction='i';
            ca.dates_only=true;
            ca.inbound_message=parseMessage(i.Parameters__c);
            if(ca.inbound_message!=null&&ca.inbound_message!='') {
                ca.parseInboundMessage();
                intUpdates.add(new Integration__c(Id=i.Id, Reprocess_Date_Time__c=DateTime.now()));
            }
        }
        if(!intUpdates.isEmpty()) {
            update intUpdates;
        }
    }
    private String parseMessage(String s) {
        String new_string;
        System.debug('Start String: ' + s);
        try {
            String search_term = 'Parameter (string):';
            String search_term_2 = '-Headers-';
            
            Integer index = s.indexOf(search_term);
            System.debug('Index 1: ' + index);
            Integer index_2 = s.indexOf(search_term_2, index);
            System.debug('Index 2: ' + index_2);
            
            new_string=s.substring(index+search_term.length(), index_2).replaceAll('\n','');
            
        } catch(Exception e) { 
            System.debug('Error w/ String: ' + e);
            new_string=null;
        }
        System.debug('Final String: ' + new_string);
        return new_string;
    }
    global void finish(Database.BatchableContext bc) { }
    
}