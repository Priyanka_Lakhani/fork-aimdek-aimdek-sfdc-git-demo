public class MBC_Integration{
    public Case c { get; set; }
    public Id cid { get; set; }
    public CA__c settings { get; set; }
    public String direction { get; set;}
    Map<String, String> headers;
    public String inbound_message { get; set; }
    public String index_separater { get; set; }
    public Boolean dates_only { get; set;}
    public Integer field_index_offset { get; set; }
    public boolean isAAAMsg {get;set;}
    public boolean isMTMsg {get;set;}
    public String page_status { get; set; }
    public String page_message { get; set; }
    public Boolean D2000 { get; set;}
    Integer INDEX_COUNT;// = 288;
    Map<String, Integration_Field_Index__c> fMap;
    List<Integration_Field_Index__c> fields;
    public String message_type { get; set;}
    public String club_number { get; set;}
    public String param_string {get;set;}
    public String host { get; set;}
    public String port { get; set;}
    public String final_url { get; set; }
    public String full_url { get; set; }
    public List<String> payload { get; set; }
    public List<Field> fieldValues { get; set; }
    String unknown_api_field = '<Unknown>';
    String space = '%20';
    String starter = '%03';
    public string responseString {get;set;}
    
    public class Field {
        public String api_name { get; set; }
        public String value { get; set; }
        public Integer index { get; set; }
        public Field (Decimal i, String a, String b) {
            index=Integer.valueOf(i);
            api_name=a;
            value=b;
        }
    }
    public MBC_Integration(){
        settings = CA__c.getOrgDefaults();
       // cid = ApexPages.currentPage().getParameters().get('Id');
    }
   
    public void cancelCall(){ 
        list<string> errs = new list<string>();
        try{   
            message_type = '21';            
            cid = ApexPages.currentPage().getParameters().get('Id');
            String reason = ApexPages.currentPage().getParameters().get('reason');
            direction = 'o';
            Map<String, String> m1 = new Map<String, String>();
            m1.put('X-Salesforce-SIP','Rest API');
            headers=m1;
            index_separater = '%fe';
            dates_only = false;
            field_index_offset=2;
            mbcAuthorize();  
            Boolean updateCase = (final_url!=NULL&&!final_url.startsWithIgnoreCase('error'));
            String new_status = settings.XX_Status__c;
            if((updateCase && new_status!=NULL&&new_status!='')) {
                c.Status=new_status;
                c.Close_Reason_Non_Roadside__c=reason;
                c.XX_Status_Date_Time__c=System.now(); 
                update c;  
            }               
        } catch(Exception e) { 
              System.debug('Error Initializing: ' + e); 
              errs.add(e.getMessage());
              AAAErrorResponse eresp = new AAAErrorResponse();
              eresp.id = GlobalClass.NewGUID();
              eresp.timestamp = string.valueof(datetime.now().yearGmt())+'-'+string.valueOf(datetime.now().monthGmt()) +'-'+
                          string.valueof(datetime.now().dayGmt())+'T'+string.valueOf(datetime.now().hourGmt()) +':' +    
                          string.valueof(datetime.now().minuteGmt())+':'+string.valueOf(datetime.now().secondGmt()) +'Z';                               
              eresp.errors = errs;
              responseString = JSON.serializePretty(eresp);
        } 
    }
    // MBC Changes START
   public MBC_Integration(Id caseid,String messageType) { 
   
        settings = CA__c.getOrgDefaults();
        System.debug('message_type before setting constructor' + message_type );
        System.debug('message_type from constructor' + messageType);
       try {
            cid = caseid;
            message_type = messageType;
            direction = 'o';
            Map<String, String> m1 = new Map<String, String>();
            m1.put('X-Salesforce-SIP','Rest API');
            headers=m1;
            index_separater = '%fe';
            
        dates_only = false;
        field_index_offset=2;
        
             
        } catch(Exception e) { System.debug('Error Initializing: ' + e); }     
    }
    
    public void mbcAuthorize(){
        System.debug('mbcAuthorize called and message type:'+message_type);
        isAAAMsg = false;
        isMTMsg = false;
        page_status = 'OK';
        page_message = '';
        
        refresh();
            
        String cNum = c.Club_Code__c;
        D2000 = IP__c.getAll().get(cNum).D2000__c;

        if(D2000) {
            INDEX_COUNT=Integer.valueOf(settings.Integration_D2000_Index_Limit__c);
        } else {
            INDEX_COUNT=Integer.valueOf(settings.Integration_Campana_Index_Limit__c);
        }
        
        buildFields(null);
        
        buildOutboundMBCMessage();
    }
    public void refresh() {
        String caseFields='';
        Set<String> fields = new Set<String>();
        for(Schema.SObjectField f : Case.SObjectType.getDescribe().fields.getMap().values()) {
            fields.add(f.getDescribe().getName());
        }
        for(String cc : Club_Comment__c.getAll().keySet()) {
            fields.add(cc);
        }
        for(String s : fields) {
            caseFields += (s + ',');
        }
        String query = 'Select ' + caseFields + ' VIN__r.Name,VIN__r.Base_Warranty_Max_Odometer__c,VIN__r.Extended_Warranty_Max_Odometer__c,VIN__r.Promo_Max_Odometer__c,Program__r.sponsorid__c,Program__r.programId__c,Program__r.Program_Code__c,mobiletech__r.account_number__c,mobiletech__r.Dispatch_Phone__c from Case where Id=\''+cid+'\' LIMIT 1';
        System.debug('REFRESH QUERY: ' + query);
        c = database.query(query);
        System.debug('case: ' + c);
    }
    public void buildFields(List<String> limits) {
        System.debug('BUILDING FIELDS: ' + limits);
        fMap = Integration_Field_Index__c.getAll();
        fields = new List<Integration_Field_Index__c>();
        
        if(limits==NULL) {
            for(Integer i = field_index_offset; i<INDEX_COUNT; i++) {
                String index = String.valueOf(i);
                if(fMap.get(index)!=NULL) {
                    fields.add(fMap.get(index));
                } else {
                    fields.add(new Integration_Field_Index__c(Index__c=i));
                }
            }
        } else {
            for(String st : limits) {
                if(fMap.get(st)!=NULL) {
                    fields.add(fMap.get(st));
                } else {
                    fields.add(new Integration_Field_Index__c(Index__c=Integer.valueOf(st!=NULL?st:'0')));
                }
            }
        }
        System.debug('BUILDING FIELDS DONE: ' + fields);
    }
    public Integer convertDateInteger(Date d) {
        return Date.newInstance(1967, 12, 31).daysBetween(d);
    }
    public Integer convertDateInteger(DateTime d) {
        return convertDateInteger(Date.newInstance(d.year(), d.month(), d.day()));
    }
    public static Integer convertDateTimeInteger(DateTime d1, Decimal offset) {
        DateTime d = d1.addHours(Integer.valueOf(offset==NULL?0:offset));//.addHours(Integer.valueOf(c.Time_Offset__c));
        
        Integer h = d.hour()*3600;
        Integer m = d.minute()*60;
        Integer s = d.second();
        
        System.debug('ENCODING TIME - H: ' + h + ', M: ' + m + ', S: ' + s);
        return (h+m+s);
    }
    public static String removeSpecialCharacters(String s, String regEx, String replaceChar) {
        if(s==null||s=='') { return ''; }
        return s.replaceAll(regEx,replaceChar);
    }
    public void buildOutboundMBCMessage() {
        if(message_type==NULL||message_type=='') { message_type = '20'; }
        if(club_number==NULL||club_number=='') { club_number = c.Club_Code__c; }
        
        try { isAAAMsg = IP__c.getAll().get(club_number).AAA__c; }  catch(Exception e) {isAAAMsg = false;}
          system.debug('isAAAMsg:' + isAAAMsg);
          
        if (isAAAMsg)
          buildOutboundMBCAAAMessage();
        else
            buildOutboundMBCCAAMessage();
          
    }
    private void buildOutboundMBCAAAMessage() {
        
        Integration__c integration = new Integration__c(Parameters__c=('Time: '+System.now()+'\n\n'+param_string),IP__c=headers.get('X-Salesforce-SIP'));
        integration.Case__c=cid;
        //insert integration;
        
        
        host = IP__c.getAll().get(club_number).IP__c;
        port = IP__c.getAll().get(club_number).Port__c;
        
        String url_path = '/api/csi';   //message_type = '20'
 
        param_string = '';
        
        Map<integer,string> AAAMsg = new Map<integer,string>();
        Map<integer,string> AAAResp =new Map<integer,string>();
        
        //new service call, or update service call
        if(message_type == '20' || message_type == '23') {
        
          AAACreate create = new AAACreate();
          
          AAACallDetails callDetails = new AAACallDetails();
          
          callDetails.sponsorId = c.program__r.sponsorId__c;
          callDetails.programId = c.program__r.programId__c;
          callDetails.goodwillCoverage = false;
          callDetails.paymentCode = 'S';
          callDetails.telephone =CA__c.getOrgDefaults().ClubautoPhone__c;
          
          
          try
          {
            callDetails.maxMileageCovered = 0;
                                                            
            if (c.vin__r.Extended_Warranty_Max_Odometer__c!=null)                                                                                                                                  
            {
              callDetails.maxMileageCovered = Integer.valueof(c.vin__r.Extended_Warranty_Max_Odometer__c);  
            }
            
        if (c.vin__r.Base_Warranty_Max_Odometer__c!=null)
        {
          if (callDetails.maxMileageCovered < Integer.valueof(c.vin__r.Base_Warranty_Max_Odometer__c))
                callDetails.maxMileageCovered = Integer.valueof(c.vin__r.Base_Warranty_Max_Odometer__c);
        }
            
        if (c.vin__r.Promo_Max_Odometer__c!=null)
        {
          if (callDetails.maxMileageCovered < Integer.valueof(c.vin__r.Promo_Max_Odometer__c))
                callDetails.maxMileageCovered = Integer.valueof(c.vin__r.Promo_Max_Odometer__c);
        }
        
        if (callDetails.maxMileageCovered == 0)
          callDetails.maxMileageCovered = 80000;
          }
          catch(System.exception exm)
          {
            callDetails.maxMileageCovered = 80000;
          }
          
          
          AAACustInfo custInfo = new AAACustInfo();
      custInfo.customerId = c.casenumber;
      custInfo.lastName = c.Last_Name__c;
      custInfo.firstName = c.first_name__c;
      if (custInfo.lastName  != null && custInfo.lastName.length() > 20)
        custInfo.lastName = custInfo.lastName.substring(0,20);
 
      if (custInfo.firstName != null && custInfo.firstName.length() > 15)
        custInfo.firstName = custInfo.firstName.substring(0,15);
        
      custInfo.contactPhone = c.phone__c;
      if (custInfo.contactPhone != null && custInfo.contactPhone != '')
          custInfo.contactPhone = c.phone__c.replace('-','').replace(')','').replace('(','').replace(' ','');
      
      if (c.phone__c == null || c.phone__c== '')
          custInfo.contactPhone = '8888888888';
      
      if (c.Phone_Type__c == 'Cellular')
        custInfo.smsUpdates = true;
      else
        custInfo.smsUpdates = false;
      custInfo.comments = getCaseComments();
      if (custInfo.comments.length()>240)
        custInfo.comments = custInfo.comments.substring(0,239);
        
      if (c.location_code__c == null || c.location_code__c=='')
        custInfo.customerLocation='DriveWay';
      else
        custInfo.customerLocation=c.location_code__c;
      
      custInfo.totalPassengers = 1;
                                                                                               
          AAAVehicle vehicleInfo = new AAAVehicle();
          vehicleInfo.make = c.Vehicle_Make__c;
          vehicleInfo.model = c.Vehicle_Model__c;
          vehicleInfo.color = c.Vehicle_Colour__c;
          if (vehicleInfo.color.length() > 8)
            vehicleInfo.color = vehicleInfo.color.substring(0,8).trim();
          try
          {
            vehicleInfo.odometer = integer.valueof(c.Current_Odometer__c);
          }
          catch(system.exception ex)
          {
            vehicleInfo.odometer = 80000;
          }
          
          if (callDetails.maxMileageCovered < vehicleInfo.odometer)
            callDetails.goodwillCoverage = true;
          
          try
          {
            vehicleInfo.year = integer.valueof(c.Vehicle_Year__c);
          }
          catch(system.exception ex)
          {
            vehicleInfo.year = 2017;
          }
          vehicleInfo.vin = c.VIN_Member_ID__c;
          if (vehicleInfo.vin.length() > 17)
            vehicleInfo.vin =vehicleInfo.vin.substring(0,17).trim();
          custInfo.vehicleInfo = vehicleInfo;
          
          callDetails.customerInfo = custInfo;
      ////////////
 
          AAARoadServiceInfo rsaInfo = new AAARoadServiceInfo();
          if (message_type == '23')
          {
            rsaInfo.callId = integer.valueof(c.Club_Call_Number__c);
          }
          
          rsaInfo.problemCode = getAAATCode();
          if (rsaInfo.problemCode == 'T6')
            rsaInfo.problemSubtype = 'Flatbed Preferred';
    
          if (c.Trouble_Code__c =='Accident Tow')
            rsaInfo.accidentTow = true;
          else
            rsaInfo.accidentTow = false;
              
          rsaInfo.dispatchCenterId = c.DsptCenter__c;
          //rsaInfo.pacesetterCodes = getPaceSetterCodes();
          decimal[] latlon = new decimal[]{decimal.valueof(0),decimal.valueof(0)};
          try
          { 
            latlon = new decimal[]{c.Breakdown_Location__longitude__s,c.Breakdown_Location__latitude__s};
          }
          catch(System.exception ex){}
          
      AAATowBreakdown towbreakdown = new AAATowBreakDown();
      towbreakdown.landmark = c.landmark__c;
      
      string stnum = '';
      string stname = c.street__c;
      
      string[] street = c.street__c.split(' ');
      if (street.size() > 1)
      {
       try
        {
          if (street[0].isNumeric() && (!street[1].isNumeric()))
          {
            stnum = street[0];
            stname = stname.removestart(street[0] + ' ');
          }
        }
        catch(System.exception ex2){}
      }
      
        towbreakdown.StreetNumber = stnum;
        towbreakdown.StreetName =stname;
        towbreakdown.CrossStreet =c.Cross_Street__c;
        towbreakdown.City =c.city__c;
        towbreakdown.State =c.Province__c;
        towbreakdown.Coordinates =latlon;
      rsaInfo.breakdownInformation = towbreakdown;
      
 
          AAATowDestination towdest = new AAATowDestination();
      towdest.facilityName = c.Destination_Name__c;
      towdest.StreetAddress = c.Destination_Street__c;
      towdest.City = c.Destination_City__c;
          towdest.State = c.Destination_Province__c;
 
          rsaInfo.towDestination = towdest;
          callDetails.roadserviceInfo = rsaInfo;
          
          create.callDetails = calldetails;
          param_string =  JSON.serializePretty(create);
          
      integration.Message__c = 'Dispatch';
          
          system.debug(param_string);
          
        }
        
        //cancel service call
        if(message_type == '21') {
          //url_path = '/api/csi';
          AAACancel cancel = new AAACancel();
          if (c.Remote_Acknowledgement__c != null)
             cancel.callInfo.callDate=GlobalClass.getDatayyyyMMdd(c.Remote_Acknowledgement__c);
           else
             cancel.callInfo.callDate=GlobalClass.getDatayyyyMMdd(c.createddate);
          try
          {
            cancel.callInfo.callId = integer.valueof(c.Club_Call_Number__c);
          }
          catch(System.exception ex){}
          cancel.callInfo.ServicingClub = c.ServicingClub__c;
          
          param_string =  JSON.serializePretty(cancel);
      integration.Message__c = 'Cancel';
          
        }
        String end_point = host+(port!=NULL?+(':'+port):'')+url_path;
       integration.Parameters__c=(integration.Parameters__c+'\n\n'+'End Point: ' +  end_point +'\n\nRequest:' + param_string);
        //upsert integration;
        //outbound field change
        //if(message_type == '23') {
        //}
        
        //string test1='';
        HttpRequest req = new HttpRequest();

        req.setEndpoint(end_point);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(30000);
        req.setBody(param_string);
    string rtnmsg = '';        
    boolean iserror = false;
    
        try {
            Http http = new Http();
            HTTPResponse res;//
            //if(!String.valueOf(userInfo.getUserId()).startsWithIgnoreCase('005o0000001CuPi')) {  
           // if(!test.IsRunningTest()){       
                res = http.send(req);
           // }
 
      rtnmsg += res.getbody();
      //test1 = res.getbody();
      if(message_type == '20') 
      {
        try
        {
          AAAError aerr = (AAAError)JSON.deserialize(res.getbody(),AAAError.class);
          if(aerr.errors != null)
          {
            iserror = true;
            //rtnmsg += '\n\n'+aerr;
          }
          else
          {
            try
            {
                                                                             
              AAACreateCallSuccess success = (AAACreateCallSuccess)JSON.deserialize(res.getbody(),AAACreateCallSuccess.class);
              
              system.debug('success:' + success);
              
              c.Last_Club_Update__c=System.now();
              c.Club_Call_Number__c = string.valueof(success.callId);
              c.ServicingClub__c = success.servicingClub;
              c.DI_Status_Date_Time__c=DateTime.now();
                  
              MBCUpdateStatusCallout(c.Id);    
              update c;
            }
            catch(System.exception ex)
            {
              iserror = true;
                                                 
              rtnmsg += '\n\n' + ex.getMessage();
                                                                
            }
          }
        }
        catch(System.exception ex){rtnmsg += '\n\n' +  ex.getMessage();iserror=true;}
        
      }
 
      if(message_type == '21') 
      {
        try
        {
          AAAError aerr = (AAAError)JSON.deserialize(res.getbody(),AAAError.class);
          if(aerr.errors != null)
          {
            iserror = true;
            rtnmsg += '\n\n' + aerr;
            AAAErrorResponse eresp = new AAAErrorResponse();
            eresp.id = aerr.id;
            eresp.timestamp = aerr.timestamp;
                
            eresp.errors = aerr.errors;
            responseString = JSON.serializePretty(eresp);
          }
          else
          {
            try
            {
                                                                                
              AAACreateCallSuccess success = (AAACreateCallSuccess)JSON.deserialize(res.getbody(),AAACreateCallSuccess.class);
              
              system.debug('cancel service call success:' + success);
              
              //c.Last_Club_Update__c=System.now();
              //c.Club_Call_Number__c = string.valueof(success.callId);
              //c.ServicingClub__c = success.servicingClub;
              //update c;
            }
            catch(System.exception ex)
            {
              iserror = true;
                                                                                                                                                                                                                                                                
                                                                                                
                                                                                 
                                                                 
              rtnmsg += '\n\n' +  ex.getMessage();
                                                                
            }
          }
        }
        catch(System.exception ex){rtnmsg += '\n\n' +  ex.getMessage();iserror=true;}
        
      }
        
        } catch(Exception e) {
      rtnmsg += '\n\n' +  e.getMessage();
          iserror = true;
        }
        
        //final_url = 'Request:' + param_string;
        
        if(iserror)
        {
            integration.Parameters__c=(integration.Parameters__c+'\n\n'+'End Point: ' +  end_point +'\n\nRequest:' + param_string + '\n\n Reponse Error: ' + rtnmsg);
            final_url = 'Error: ' + rtnmsg; // + '\n\n' + test1;
            
        }
        else
        {
            integration.Parameters__c=(integration.Parameters__c+'\n\n'+'End Point: ' +  end_point +'\n\nRequest:' + param_string + '\n\nResponse Success: ' + rtnmsg);
            final_url = 'Success:' + rtnmsg;
        }
        upsert integration;
    }
    private void buildOutboundMBCCAAMessage()
    {                
    system.debug('buildOutboundCAAMessage----'+param_string+'-'+headers.get('X-Salesforce-SIP'));
       Integration__c integration = new Integration__c(Parameters__c=('Time: '+System.now()+'\n\n'+param_string),IP__c=headers.get('X-Salesforce-SIP'));
        integration.Case__c=cid;
        //insert integration;
        
        Map<String, String> translationMap = new Map<String, String>();
        for(Field_Translation__c f : Field_Translation__c.getAll().values()) {
            String key = f.Type__c + '-' + f.Club__c + '-' + f.Input__c;
            translationMap.put(key, f.Output__c);
        }
        //if(message_type==NULL||message_type=='') { message_type = '20'; }
        //if(club_number==NULL||club_number=='') { club_number = c.Club_Code__c; }
        
        host = IP__c.getAll().get(club_number).IP__c;
        port = IP__c.getAll().get(club_number).Port__c;
        //D2000 = IP__c.getAll().get(club_number).D2000__c;
        
        
        String url_path = '/cgi-bin/string_transfer?';
        String url_params = 'clb_cd=500&cons_id=316&password=passwd&dest=CAI&string=';
    
        full_url = host+':'+port+url_path+url_params;
        system.debug('full_url ----'+full_url);
        payload = new List<String>{'','','','','','','','','',''};
        
        payload[0] = 'NET';
        
        payload[1] = c.CAKEY2__c;//.replaceAll('\\%2a', '\\*');
        
        payload[2] = message_type;
        
        payload[4] = '';
        
        //new service call
        if(message_type == '20') {
        system.debug('463 : message_type ----'+message_type);
            payload[3] = '~';
            
            fieldValues = new List<Field>();
            
            for(Integration_Field_Index__c i : fields) {
                String api_name = (i.API_Name__c!=NULL?i.API_Name__c:unknown_api_field);
                String value = '';
                try { 
          if((D2000 && i.D2000_Only__c) || (!i.D2000_Only__c && !i.Campana_Only__c) || (!D2000 && i.Campana_Only__c)) {
            if(i.Convert_To_Date__c) { value=(''+convertDateInteger((Date)c.get(i.API_Name__c))); }
            else if(i.Convert_To_Time__c) { value=(''+convertDateTimeInteger((DateTime)c.get(i.API_Name__c),c.Time_Offset__c)); }
            else if(i.Requires_Translation__c!=NULL&&i.Requires_Translation__c!='') { String key = i.Requires_Translation__c+'-'+club_number+'-'+c.get(i.API_Name__c); value=translationMap.get(key); }
            else if(i.Content__c=='COMMENTS') { value=getCaseComments(); }
            else { value=(''+c.get(i.API_Name__c)); }
            
          }
                } catch(Exception e) {}
                
                
                //if (D2000 && i.index__c==2)
                //  value = c.casenumber.leftPad(17).replace(' ','0');
                  /*
                  try
                  {
                    if (string.valueof(c.get(i.API_Name__c)).contains('VIN'))
                    {
                                                                                                                                                                                                                                                                                                                                
                                                                                                 
            system.debug('i.index__c:' + i.index__c);
            system.debug('API_Name__c):' + i.API_Name__c);
            system.debug('c.get(i.API_Name__c):' + c.get(i.API_Name__c));
            system.debug('value:'+ value);
                                                                                                
                    }
                  }
                  catch(System.exception ex){}
                */
                
                /*Start Aimdek Changes GN */   
                if(i.API_Name__c == 'Notes__c') {  
                    if(c.Program__r.Program_Code__c =='582') {
                        if(c.Roadside_Purchased__c != null && c.Roadside_Purchased__c == 'No') {                        
                            value = 'CUST PAYS FOR SRVC';
                        }
                        if(c.Trouble_Code__c == 'Gas') {
                            value = 'CUST PAYS FOR GAS';
                        }
                        if(c.Trouble_Code__c == 'Gas' && c.Proceed_with_Cash_Call__c != null && c.Proceed_with_Cash_Call__c == 'YES'){
                            value = 'CUST PAYS FOR SRVC';
                        }
                        if(c.Is_Customer_Pay_Extra_cost_For_Tow__c != null && c.Is_Customer_Pay_Extra_cost_For_Tow__c == 'YES'){
                            value = 'CUST PAYS DIFFERENCE AFTER 25KM';
                        }
                    }
                }
                /* End Aimdek changes GN */
                value=removeSpecialCharacters(value,settings.Special_Char_RegEx__c,'');
                
                payload[3]+=value;
                payload[3]+='~';
                
                fieldValues.add(new Field(i.Index__c, i.API_Name__c!=NULL?i.API_Name__c:unknown_api_field, (value!=NULL&&value!=''?value:'')));
            }
            payload[3]=payload[3].replaceAll(' ', space).replaceAll('null','');
            //payload[3]=EncodingUtil.urlEncode(payload[3].replaceAll('null',''), 'UTF-8');
            //payload[3]=payload[3].replaceAll('\\%252a', '*');
            //payload[3]=payload[3].replaceAll('\\%7E', '*');
            integration.Message__c = 'Dispatch';
        }
        
        //cancel service call
        if(message_type == '21') {
            payload[3] = c.Call_ID__c;//c.Club_Call_Number__c + detail_separater + club_number + detail_separater + convertDateInteger(c.RE_Status_Date_Time__c);
            integration.Message__c = 'Cancel';
        }
        
        //outbound field change
        if(message_type == '23') {
            payload[3] = '~';
            
            fieldValues = new List<Field>();
            
            for(Integration_Field_Index__c i : fields) {
                String api_name = (i.API_Name__c!=NULL?i.API_Name__c:unknown_api_field);
                String value = '';
                try { 
                    if(i.Convert_To_Date__c) { value=(''+convertDateInteger((Date)c.get(i.API_Name__c))); }
                   else if(i.Convert_To_Time__c) { value=(''+convertDateTimeInteger((DateTime)c.get(i.API_Name__c),c.Time_Offset__c)); }
                    else if(i.Requires_Translation__c!=NULL&&i.Requires_Translation__c!='') { String key = i.Requires_Translation__c+'-'+club_number+'-'+c.get(i.API_Name__c); value=translationMap.get(key); }
                    else if(i.Content__c=='COMMENTS') { value=getCaseComments(); }
                    else { value=(''+c.get(i.API_Name__c)); }
                } catch(Exception e) {}
                
                value=removeSpecialCharacters(value,settings.Special_Char_RegEx__c,'');
                
                payload[3]+=value;
                payload[3]+='~';
                
                fieldValues.add(new Field(i.Index__c, i.API_Name__c!=NULL?i.API_Name__c:unknown_api_field, value));
            }
            payload[3]=payload[3].replaceAll(' ', space).replaceAll('null','');
            //payload[3]=EncodingUtil.urlEncode(payload[3].replaceAll(' ', space).replaceAll('null',''), 'UTF-8');
            //payload[3]=payload[3].replaceAll('\\%252a', '*');
            //payload[3]=payload[3].replaceAll('\\%252a', '*');
            integration.Message__c = 'Dispatch';
        }
        
        payload[3]=payload[3].left(payload[3].length()-1);
        
        payload[5] = '';
        payload[6] = 'D';
        payload[7] = '';
        payload[8] = '';
        payload[9] = c.Call_Centre_ID__c!=NULL?c.Call_Centre_ID__c:'500';
        
        HttpRequest req = new HttpRequest();
        String end_point = host+(port!=NULL?+(':'+port):'')+url_path+url_params+starter;
        system.debug('463 : end_point ----'+end_point);
        /*String certName;
        if(certName!=NULL) {
            req.setClientCertificateName(certName);
        }*/
        for(String s : payload) {
            end_point += (s+index_separater);
        }
        system.debug('333333333 ' + index_separater);
        system.debug('444444444 ' + end_point);
        end_point = end_point.left(end_point.length()-index_separater.length());

        req.setEndpoint(end_point);
        req.setMethod('GET');
        req.setTimeout(5000);
        try {
            Http http = new Http();
            HTTPResponse res;
            //if(!test.IsRunningTest()){
                res = http.send(req);
           // }
 
            final_url = 'Success';
            final_url+=('\n\n'+res.getBody());
            final_url+=('\n\n'+req.toString());
            integration.Parameters__c=(integration.Parameters__c+'\n\n'+final_url);
            if(message_type=='20')
                MBCUpdateStatusCallout(c.Id);
            
             if(message_type == '21') 
             {
                try{
                    CAAError aerr = (CAAError)JSON.deserialize(res.getbody(),CAAError.class);
                    if(aerr.errors != null)
                    {
                        CAAErrorResponse eresp = new CAAErrorResponse();
                        eresp.id = aerr.id;
                        eresp.timestamp = aerr.timestamp;
                            
                        eresp.errors = aerr.errors;
                        responseString = JSON.serializePretty(eresp);
                    }
                }catch(System.exception ex){}
             }
            //update integration;
        } catch(Exception e) {
            final_url = 'Error: ' + e.getMessage();
            final_url+=('\n\n'+req.toString());
            integration.Parameters__c=(integration.Parameters__c+'\n\n'+final_url);
        }
        
        upsert integration;
    }

    private void MBCUpdateStatusCallout(ID caseId){
        Case cc=[select CaseNumber, clientcasenumber__c, Current_Odometer__c, status, DI_Status_Date_Time__c, OL_Status_Date_Time__c, ER_Status_Date_Time__c, CL_Status_Date_Time__c, ETA__c,Trouble_code__c,Club_Code__c,Club__r.Name,Language__c,Phone__c,MBCProcessStep__c from case where id=:caseId];
        String dispatchedStatus = settings.DI_Status__c + ' - ' + cc.Club_code__c + ' (' + cc.Club__r.Name + ')';
        cc.Status = dispatchedStatus;
        cc.DI_Status_Date_Time__c= System.now();
        system.debug('without pretty'+JSON.serialize(cc));
        String JsonString=JSON.serialize(cc);
        
        String host = ClientServicesSetting__c.getAll().get('mercedes-benz').EndPoint_URL__c;
        String timestamp_url_path = '/mbc/updateStatus';
        String username = ClientServicesSetting__c.getAll().get('mercedes-benz').UserName__c;
        String password = ClientServicesSetting__c.getAll().get('mercedes-benz').Password__c;
        String mbc_end_point = host + timestamp_url_path;
        String reqMethod= 'POST';
        RestCallouts rc = new RestCallouts();
        
        rc.mbcCallout(mbc_end_point, username, password , JsonString,caseId, reqMethod);
    }
    
    public String getCaseComments() {
        String s = '';
        try {
            for(Club_Comment__c cc : [Select Id, Name, Program__c, Label__c, Boolean__c from Club_Comment__c ORDER BY Order__c ASC]) {
                
                System.debug('Case Field: ' + cc.Name + ' = ' + c.get(cc.Name));
                if((cc.Program__c!=NULL && !cc.Program__c.containsIgnoreCase(c.Client_Code_Formula__c)) || c.get(cc.Name)==NULL) { continue; }
                
                if((cc.Boolean__c && c.get(cc.Name)==TRUE) || (c.get(cc.Name)!=NULL && c.get(cc.Name)!=FALSE)) {
                    s+=(', '+ (String.isEmpty(cc.Label__c)?'':(cc.Label__c + ': ')) +c.get(cc.Name));
                }
            }
            if(s.length()>0) { s = s.substring(2); }
        } catch(Exception e) {
            System.debug('CLUB COMMENT ERROR: ' + e);
        }
        System.debug('CLUB COMMENTS: ' + s);
 
        if (c.JLR__c && c.trouble_code__c.contains('Tow') && c.Tow_Reason__c == CA__c.getOrgDefaults().Call_Flow_Engine_Wont_Start__c)
        {
          s += ',Skates or dollies req';
          if (c.is_noise__c == 'Yes')
            s+=',Jump to put veh in neutral';
        }
        
        if (c.trouble_code__c.contains('Tow') && c.Tow_Reason__c=='Lost Keys' && (c.client_code__c == '511' || c.client_code__c == '512' || c.client_code__c == '531' || c.client_code__c == '532'))
        {
          s += ',Tow can be provided at customers expense';
        }        
        return s;
    }
    private string getAAATCode()
    {
    /*
        CASE(TEXT(Trouble_Code__c),
      "Install Spare", "T1",
      "Boost", "T3",
      "Gas", "T4",
      "Winch", "T5",
      "Tow", "T6",
      "Flatbed Tow", "T6F",
      "Heavy Duty Tow", "T6H",
      "Unlock", "T7",
      "Accident Tow", "T8",
      "Locksmith", "T20",
      "Exception", "T9",
      "Miscellaneous Mechanical Call", "T11",
      NULL */
        string rtn = c.T_Code__c;
     
        if (c.T_Code__c.StartsWith('T6'))
          rtn = 'T6';
     
        if (c.Trouble_Code__c == 'Gas')
          rtn = 'T5';
     
        if (c.Trouble_Code__c == 'Winch')
          rtn = 'T8';
     
        if (c.Trouble_Code__c == 'Locksmith')
          rtn = '7A';
      
        if (c.Trouble_Code__c =='Miscellaneous Mechanical Call')
          rtn = 'T9';
     
        if (c.Trouble_Code__c =='Accident Tow')
          rtn = 'T6';
     
        return rtn;
    }
    
    public class AAACreate
    {
      public string serviceType = 'CreateRAPCall';
      public AAACallDetails callDetails {get;set;}
      public AAACreate()
      {
        callDetails = new AAACallDetails();
      }
    }
    public class AAACallDetails
    {
      //public integer callId {get;set;}
      public string sponsorId {get;set;} 
      public string programId {get;set;} 
      public integer maxMileageCovered {get;set;}
      public boolean goodwillCoverage {get;set;}
      public AAACustInfo customerInfo {get;set;}
      public AAARoadServiceInfo roadserviceInfo {get;set;}
      public string paymentCode {get;set;}
      public string telephone {get;set;}
    }
    public class AAACustInfo
    {
      public string customerId {get;set;}
      public string lastName {get;set;}
      public string firstName {get;set;}
      public string contactPhone {get;set;}
      public boolean smsUpdates {get;set;}
      public string comments {get;set;}
      public string customerLocation {get;set;}
      public integer totalPassengers {get;set;}
      public AAAVehicle vehicleInfo {get;set;}
    }
    public class AAARoadServiceInfo
    {
      public integer callId {get;set;}
      public string problemCode {get;set;}
      public string problemSubtype {get;set;}
      //public string[] pacesetterCodes {get;set;}
      public boolean accidentTow {get;set;}
      public string dispatchCenterId {get;set;}
      public AAATowBreakdown breakdownInformation {get;set;}
      public AAATowDestination towDestination {get;set;}
      
      public AAARoadServiceInfo()
      {
        towDestination = new AAATowDestination();
        breakdownInformation = new AAATowBreakdown();
      }
    }
    public class AAAVehicle
    {
      public string make {get;set;}
      public string model {get;set;}
      public string color {get;set;}
      public integer year {get;set;}
      public string vin {get;set;}
      public integer odometer {get;set;}
      public string tagNumber {get;set;} 
    }
    public class AAATowBreakdown
    {
      public string landmark {get;set;}
      public string streetNumber {get;set;}
      public string streetName {get;set;}
      public string crossStreet {get;set;}
      //public string highway {get;set;}
      //public string exitNumber {get;set;}
      public string city {get;set;}
      public string state {get;set;}
      //public string postalCode {get;set;}
      public decimal[] coordinates {get;set;}
    }
    
    public class AAATowDestination
    {
      public string facilityName {get;set;}
      public string streetAddress {get;set;}
      public string city {get;set;}
      public string state {get;set;}
      //public string postalCode {get;set;}
    }
    public class AAACancel 
    {
        public string serviceType = 'CancelRAPCall';
        public AAACancelCallInfo callInfo {get;set;}
        public AAACancel(){
          callInfo = new AAACancelCallInfo();
        }
    }
    public class AAACancelCallInfo
    {
      public string callDate {get;set;}
      public integer callId {get;set;}
      public string servicingClub {get;set;}
    }
    public class AAAError
    {
      public string id {get;set;}
      public string timestamp {get;set;}
      public list<string> errors {get;set;}
    }
    
    public class CAAError
    {
      public string id {get;set;}
      public string timestamp {get;set;}
      public list<string> errors {get;set;}
    }

   public class AAAResponse
   {
     public string id {get;set;}
     public string timestamp {get;set;} 
   }

   public class AAAErrorResponse
   {
     public string id {get;set;}
     public string timestamp {get;set;}
     public list<string> errors {get;set;} 
   }
   public class CAAErrorResponse
   {
     public string id {get;set;}
     public string timestamp {get;set;}
     public list<string> errors {get;set;} 
   }
   public class AAACreateCallSuccess
    {
      public string id {get;set;}
      public string timestamp {get;set;}
      public integer callId {get;set;}
      public string callDate {get;set;}
      public string servicingClub {get;set;}
      public string callStatus {get;set;}
      public string promisedArrivalWindow {get;set;}
    }
    // MBC Changes END   
}