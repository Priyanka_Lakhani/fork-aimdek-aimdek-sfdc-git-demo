@RestResource(urlMapping='/RESTInboundMTMessageService/*')
global with sharing class RESTInboundMTMessageService {
  @HttpPost
  global static void startService() {
    RestContext.response.addHeader('Content-Type', 'application/json');
      RestRequest req = RestContext.request;
      ResponseService resp = new ResponseService();    
    RequestService reqObj = (RequestService)JSON.deserialize(req.requestBody.toString(),RequestService.class);    
    Boolean haserr = false;
        Boolean isMTdenied = false;
    Integration__c integration = new Integration__c(Parameters__c=('Time: '+System.now()+'\n\n'+JSON.serializePretty(reqObj, true)), IP__c=req.remoteAddress);        
    if(reqObj.Type == 'GETMTInfo') {
      Date lstupd = reqObj.LastUpdatedDate;
      List<Account> accs = [SELECT id,account_number__c,firstname__c,
                     lastname__c,servicingpostalcode__c,Location__Latitude__s,
                     Location__Longitude__s,billingstreet,billingcity,
                     billingstate,billingcountry,billingpostalcode,
                     Dispatch_Email__c,Dispatch_Phone__c,Monday__c,
                     Tuesday__c,Wednesday__c, Thursday__c,
                     Friday__c,Saturday__c,Sunday__c,Status__c 
                  FROM 
                    Account 
                  WHERE 
                    type = 'Mobile Tech' and 
                    lastmodifieddate > :lstupd];
                    
                    
                    
      List<MTInfo> infs = new List<MTInfo>();      
      for(Account a : accs) {
        MTInfo inf = new MTInfo();        
              inf.MTID =a.account_number__c;
              inf.FirstName = a.firstname__c;
              inf.LastName =a.lastname__c;
              inf.phone=a.dispatch_phone__c;
              inf.email = a.dispatch_email__c;
              inf.street = a.billingstreet;
              inf.city = a.billingcity;
              inf.state=a.billingstate;
              inf.country=a.billingcountry;
              inf.postalcode = a.billingpostalcode;
              if (a.status__c == 'Inactive') {
                inf.isActive = false;
              } else { 
                inf.isActive=true;
              }
                inf.servicingpostalcode = a.servicingpostalcode__c;
                inf.Lat = a.Location__Latitude__s;
                inf.Lon = a.Location__Longitude__s;          
              if(a.Monday__c != null && a.Monday__c != '') {
                  try {                                                                           
                    string[] ss = a.Monday__c.split('-');
                    inf.MonStart = ss[0];
                    inf.MonEnd = ss[1];
                  } catch(System.exception ex) {                   
                  }
              }
              if(a.Tuesday__c != null && a.Tuesday__c != '') {
                  try  {
                    string[] ss = a.Tuesday__c.split('-');
                    inf.TueStart = ss[0];
                    inf.TueEnd = ss[1];
                  } catch(System.exception ex) {
                  }
              }
               if(a.Wednesday__c != null && a.Wednesday__c != '') {
                   try {
                       string[] ss = a.Wednesday__c.split('-');
                       inf.WedStart = ss[0];
                       inf.WedEnd = ss[1];
                   } catch (System.exception ex) {

                   }
               }
               if(a.Thursday__c != null && a.Thursday__c != '') {
                   try {
                       string[] ss = a.Thursday__c.split('-');
                       inf.ThuStart = ss[0];
                       inf.ThuEnd = ss[1];
                   } catch (System.exception ex) {
                   }
               }
               if(a.Friday__c != null && a.Friday__c != '') {
                   try {
                       string[] ss = a.Friday__c.split('-');
                       inf.FriStart = ss[0];
                       inf.FriEnd = ss[1];
                   } catch (System.exception ex) {
                   }
               }
               if(a.Saturday__c != null && a.Saturday__c != '') {
                   try {
                       string[] ss = a.Saturday__c.split('-');
                       inf.SatStart = ss[0];
                       inf.SatEnd = ss[1];

                   } catch (System.exception ex) {
                   }
               }
               if(a.Sunday__c != null && a.Sunday__c != '') {
                   try {
                       string[] ss = a.Sunday__c.split('-');
                       inf.SunStart = ss[0];
                       inf.SunEnd = ss[1];
                   } catch (System.exception ex) {
                   }
               }              
              infs.add(inf);        
      }      
      Blob body = Blob.valueOf(JSON.serializePretty(infs, true));
        RestContext.response.responseBody = body;                              
    }     
    if(reqObj.Type == 'MTAck') {    
      try {
        Case c = [SELECT id,status,Club_Call_Number__c,
                CaseNumber,Last_Integration_Update__c,RE_Status_Date_Time__c,
                Remote_Acknowledgement__c,ER_Status_Date_Time__c,OL_Status_Date_Time__c,
                CL_Status_Date_Time__c,DsptCenter__c 
             FROM Case 
             WHERE 
               casenumber = :string.valueof(reqObj.CallDetails.CaseNumber) 
             limit 1];
        c.Last_Integration_Update__c = JSON.serializePretty(reqObj, true);  
        c.status = CA__c.getOrgDefaults().DI_Status__c;
        c.Remote_Acknowledgement__c=datetime.now();
        c.Last_Club_Update__c = datetime.now();
        c.Club_Call_Number__c = c.CaseNumber;
        update c;
        resp.Status = 'OK'; 
        integration.message__c = 'reqObj.Type';
        insert integration;
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
          RestContext.response.responseBody = body;
      } catch (Exception ex) {
        resp.Status = 'Error';
        resp.Error = 'No Case Number Found';
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
          RestContext.response.responseBody = body;      
      }           
    }   
    if(reqObj.Type == 'MTCallInfo') {
      try {
        Case c = [SELECT id,status,Last_Integration_Update__c,
                 RE_Status_Date_Time__c,DsptCenter__c 
              FROM Case 
              WHERE 
                Club_Call_Number__c = :reqObj.CallDetails.CaseNumber 
              limit 1];  
        c.Last_Integration_Update__c = JSON.serializePretty(reqObj, true);
              if(reqObj.CallDetails.Status == 'RE') {  
                  c.status = CA__c.getOrgDefaults().RE_Status__c;
                  c.RE_Status_Date_Time__c = DateTime.now();
              }  
              if(reqObj.CallDetails.Status == 'DI') {
                  c.status = CA__c.getOrgDefaults().DI_Status__c;
                  c.DI_Status_Date_Time__c = DateTime.now();
              }  
              if(reqObj.CallDetails.Status == 'ER') {
                  c.status = CA__c.getOrgDefaults().ER_Status__c;
                  c.ER_Status_Date_Time__c = DateTime.now();
              }  
              if(reqObj.CallDetails.Status == 'OS') {
                  c.status = CA__c.getOrgDefaults().OL_Status__c;
                  c.OL_Status_Date_Time__c = DateTime.now();
              }
              if(reqObj.CallDetails.Status == 'UT') {
                  c.status = CA__c.getOrgDefaults().OL_Status__c;
              }
              c.Last_Club_Update__c = datetime.now();
              integration.message__c = reqObj.Type + ' - ' + reqObj.CallDetails.Status;    
              insert integration;
              update c;
              resp.Status = 'OK'; 
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
          RestContext.response.responseBody = body;                          
      } catch (Exception e) {
        resp.Status = 'Error';
        resp.Error = 'No Case Number Found';
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
          RestContext.response.responseBody = body;          
      }
    }    
    if(reqObj.Type == 'CloseCall') {
      try {
        Case c = [SELECT id
              FROM Case 
              WHERE 
                casenumber = :reqObj.CallDetails.CaseNumber 
              limit 1];
        
            String caseFields='';
            Set<String> fields = new Set<String>();
            for(Schema.SObjectField f : Case.SObjectType.getDescribe().fields.getMap().values()) {
                fields.add(f.getDescribe().getName());
            }
            for(String cc : Club_Comment__c.getAll().keySet()) {
                fields.add(cc);
            }
            for(String s : fields) {
                caseFields += (s + ',');
            }
            String query = 'Select ' + caseFields + ' VIN__r.Name,VIN__r.Base_Warranty_Max_Odometer__c,VIN__r.Extended_Warranty_Max_Odometer__c,VIN__r.Promo_Max_Odometer__c,Program__r.sponsorid__c,Program__r.Program_Code__c,Program__r.programId__c,mobiletech__r.account_number__c,mobiletech__r.Dispatch_Phone__c from Case where Id=\''+c.id+'\' LIMIT 1';
            System.debug('REFRESH QUERY: ' + query);
            c = database.query(query);    
            c.Last_Integration_Update__c = JSON.serializePretty(reqObj, true);
              if(reqObj.CallDetails.Notes !=null) {                                                      
                if (reqObj.CallDetails.Notes.length() > 50) {
                  c.MTCloseReason__c = reqObj.CallDetails.Notes.substring(0,50);
                } else {
                  c.MTCloseReason__c = reqObj.CallDetails.Notes;
                }                                                                                                                                                                                                                       
              }
              c.Last_Club_Update__c = datetime.now();    
              if(reqObj.CallDetails.Reason != 'Cleared' && reqObj.CallDetails.Reason!= 'GOA') {
                c.MObTechDenied__c = 'Yes';
                if(reqObj.CallDetails.Reason == 'MT Not Available' || reqObj.CallDetails.Reason == 'MT Rejected' ) {
                  c.Status = c.status = CA__c.getOrgDefaults().DI_Status__c;
                } else {
                  c.Status = c.status = CA__c.getOrgDefaults().SP_Status__c;
                }
                c.Club_Call_Number__c = '';
                c.MTDenyReason__c =reqObj.CallDetails.Reason;
              } else {
                c.status = 'Closed (Call Cleared)';
                if(reqObj.CallDetails.Reason == 'GOA') {
                    c.MTCloseReason__c =reqObj.CallDetails.Reason;
                    c.Kill_Code__c = reqObj.CallDetails.Reason;
                    c.Clear_Code__c = 'Gone on Arrival';                  
                }
              }  
              integration.message__c = 'MTCloseCall';
              insert integration;      
            update c;        
              resp.Status = 'OK'; 
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
          RestContext.response.responseBody = body;        
      } catch(Exception e) {
        resp.Status = 'Error';
        resp.Error = 'No Case Number Found';
        Blob body = Blob.valueOf(JSON.serializePretty(resp, true));
          RestContext.response.responseBody = body;        
      }
    }   
  }   
  
    global class MTInfo {
      public String MTID {get;set;}
      public String FirstName {get;set;}
      public String LastName {get;set;}
      public String Phone {get;set;}
      public String Email {get;set;}
      public String Street {get;set;}
      public String City {get;set;}
      public String State {get;set;}
      public String Country {get;set;}
      public String PostalCode {get;set;} 
      public decimal Lat {get;set;}
      public decimal Lon {get;set;}
      public String MonStart {get;set;}
      public String MonEnd{get;set;}
      public String TueStart {get;set;}
      public String TueEnd{get;set;}
      public String WedStart {get;set;}
      public String WedEnd{get;set;}
      public String ThuStart {get;set;}
      public String ThuEnd{get;set;}
      public String FriStart {get;set;}
      public String FriEnd{get;set;}
      public String SatStart {get;set;}
      public String SatEnd{get;set;}
      public String SunStart {get;set;}
      public String SunEnd {get;set;}
      public String ServicingPostalCode {get;set;}
      public Boolean isActive {get;set;}       
    }    

    global class ResponseService {
        public String Status {get; set;}
        public String Error {get; set;}  
    }    

  public class MTCallInfoDetails
  {
      public String CaseNumber {get;set;}
      public Integer ETA {get;set;}
      public Decimal lat {get;set;}
      public Decimal lon {get;set;}
      public String Status {get;set;}
      public String Reason {get;set;}
      public String Notes {get;set;}      
  } 
    
    global class RequestService {
      public String Type {get; set;}
      public Date LastUpdatedDate {get; set;}
      public MTCallInfoDetails CallDetails {get;set;}
  }       
}