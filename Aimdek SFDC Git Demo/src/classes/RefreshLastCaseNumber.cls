global class RefreshLastCaseNumber implements Schedulable{
    global void execute(SchedulableContext sc) {
        Case c = [Select Id, CaseNumber from Case where CreatedDate=LAST_N_DAYS:14 ORDER BY CaseNUmber DESC LIMIT 1];
        CA__c settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c=c.CaseNumber;
        update settings;
        //update [Select id from Account where RecordType.DeveloperName='Dispatch_Club'];
    }
}