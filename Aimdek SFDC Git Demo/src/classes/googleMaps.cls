public class googleMaps {

    public String duration {get;set;}
    public Integer travelTime {get;set;}
    public Decimal distance {get;set;}
    public Decimal distanceInMiles {get;set;}
    public class Distance
    {
        public string text { get; set; }
        public integer value { get; set; }
    }
    
    public class Duration
    {
        public string text { get; set; }
        public integer value { get; set; }
    }
    
    public class Element
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }
    
    public class Row
    {
        public Element[] elements { get; set; }
    }
    
    public class Parent
    {
        public string[] destination_addresses { get; set; }
        public string[] origin_addresses { get; set; }
        public Row[] rows { get; set; }
        public string status { get; set; }
    }
    
    public googleMaps(String address1,String address2) {
         system.debug('add1:'+address1+'    add2:'+address2);
         String jsonResults = getJsonResults(address1, address2);
         system.debug('jsonresults:'+jsonResults);
         setDistanceResponse(jsonResults);
         setDistanceResponseInMiles(jsonResults);
    }

    public String getJsonResults(String address1,String address2) {
        system.debug('add1:'+address1+'    add2:'+address2);
        String jsonResults = '';
        try{        
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            
            req.setMethod('GET');
            
            String url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
                + '?origins=' + address1
                + '&destinations=' + address2
                + '&mode=driving'
                + '&sensor=false'
                + '&language=en'
                + '&units=imperial'
                + '&key=AIzaSyDea1UmFTWktYnXZDK1zweG-eMILQNPpEo';
            system.debug('url:'+url);    
            req.setEndPoint(url);
            
            HTTPResponse resp = http.send(req);
            
            jsonResults = resp.getBody().replace('\n', '');
        }catch(System.Exception ex){
            System.debug('Distance calculation Exception:'+ex);
        }
        return jsonResults;
    }
    
    public void setDistanceResponse(String jString){
        Parent res = (Parent)JSON.deserialize(jString,Parent.class);
        system.debug('rows:'+res.rows);
        Row[] row = res.rows;
        Element[] element = row[0].elements;
        Distance distanceObj = element[0].distance;
        distance = (Decimal.valueOf(distanceObj.value))/1000;
        System.debug('distance:'+distanceObj.text+distanceObj.value);       
    }
    public void setDistanceResponseInMiles(String jString){
        Parent res = (Parent)JSON.deserialize(jString,Parent.class);
        system.debug('rows:'+res.rows);
        Row[] row = res.rows;
        Element[] element = row[0].elements;
        Distance distanceObj = element[0].distance;
        distanceInMiles = (Decimal.valueOf(((distanceObj.text).split(' '))[0]));
        System.debug('distance:'+distanceObj.text+distanceObj.value); 
        System.debug('distanceInMiles :'+distanceInMiles );       
    }
      
}