public class ServiceTrackerIntegrationController {

    public class SecurityToken {
        public String ApplicationID { get; set; }
        public String ApplicationPassword { get; set; }
        public String ApplicationMethodName { get; set; }
        
        public SecurityToken(String a, String b, String c) {
            ApplicationID=a;
            ApplicationPassword=b;
            ApplicationMethodName=c;
        }
    }
    public class ServiceTrackerRequest {
        public ServiceTrackerRequest() { }
        public String RequestID { get; set; }
        public String ServicingClubCode { get; set; }
        public String CallNumber { get; set; }
        public SecurityToken SecurityToken { get; set; }
        public String TrackingToken { get; set; }
        public String UserIdentifier { get; set; }
    }
    
    public ServiceTrackerIntegrationController (){
    }
    
    public static List<Service_Tracker_Detail__c> getServiceTrackerCalls(Case c) {        
        
        Service_Tracker__c settings = Service_Tracker__c.getOrgDefaults();
        List<Service_Tracker_Detail__c> service_calls = new List<Service_Tracker_Detail__c>();

        ServiceTrackerRequest s = new ServiceTrackerRequest();
        
        s.ServicingClubCode = c.Club_Code__c;
        s.UserIdentifier = c.VIN_Member_ID__c;

        s.CallNumber = '';

        s.SecurityToken = new SecurityToken(settings.ApplicationID__c, settings.ApplicationPassword__c, settings.ApplicationMethodName__c);
        
        String j = JSON.serialize(s);
        
        System.debug('JSON: ' + j);
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(settings.End_Point__c);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(j); 
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug('THE RESPONSE: ' + res.getBody());
        
        JSON2Apex j2a = JSON2Apex.parse(res.getBody());     
        if(j2a!=null && j2a.calls!=null && !j2a.calls.isEmpty()) {          
            service_calls = formatServiceCalls(j2a,res);
        }
        
        return service_calls;
        
    }
    public static List<Service_Tracker_Detail__c> formatServiceCalls(JSON2Apex j2a, HTTPResponse res) {
        List<Service_Tracker_Detail__c> service_calls = new List<Service_Tracker_Detail__c>();
        
        for(JSON2Apex.Calls st_call : j2a.calls) {
                
            String uid = (st_call.CallDate.year()+'-'+st_call.CallDate.month()+'-'+st_call.CallDate.day()+'-'+st_call.ServicingClubCode+'-'+st_call.CallNumber);
            
            service_calls.add(new Service_Tracker_Detail__c(
                Call_Number__c=st_call.CallNumber,
                Call_Date__c=Date.newInstance(st_call.CallDate.year(), st_call.CallDate.month(), st_call.CallDate.day()),
                Call_State__c=st_call.CallState,
                Problem_Description__c=st_call.ProblemDescription,
                Garage_Name__c=st_call.GarageName,
                Car_Make__c=st_call.CarMake,
                Car_Model__c=st_call.CarModel,
                Car_Year__c=st_call.CarYear,
                Car_Colour__c=st_call.CarColor,
                Breakdown_Location__c=st_call.BreakDownLocation,
                Breakdown_City__c=st_call.BreakDownCity,
                Cross_Street__c=st_call.CrossStreet,
                Location_Description__c=st_call.LocationDescription,
                Direction__c=st_call.Direction,
                ETA__c=st_call.ETA,
                ETA_Date_Time__c=st_call.ETA_DateTime,
                Receive_Time__c=st_call.ReceiveTime,
                Tow_Destination__c=st_call.TowDestination,
                Breakdown_Geolocation__Latitude__s=st_call.BreakDownLatitude,
                Breakdown_Geolocation__Longitude__s=st_call.BreakDownLongitude,
                Breakdown_Accuracy__c=st_call.BreakDownAccuracy,
                Driver_Geolocation__Latitude__s=st_call.DriverLatitude,
                Driver_Geolocation__Longitude__s=st_call.DriverLongitude,
                Driver_GPS_Accuracy__c=st_call.DriverGPSAccuracy,
                Driver_GPS_Time__c=st_call.DriverGPSTime,
                Next_Update__c=st_call.NextUpdate,
                Alert_Message__c=st_call.AlertMessage,
                Servicing_Club_Code__c=st_call.ServicingClubCode,
                Device_ID__c=st_call.DeviceID,
                Device_Type__c=st_call.DeviceType,
                Driver_Code__c=st_call.DriverCode,
                Driver_Name__c=st_call.DriverName,
                Tracking_Token__c=st_call.TrackingToken,
                UID__c=uid,
                Last_Update__c=DateTime.now(),
                Last_Update_Request__c=res.getBody()
            ));
        }
        return service_calls;
    }
}