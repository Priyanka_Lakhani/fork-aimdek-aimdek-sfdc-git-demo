global class RefreshDispatchClubs implements Schedulable{
    global void execute(SchedulableContext sc) {
        update [Select id from Account where RecordType.DeveloperName='Dispatch_Club'];
    }
}